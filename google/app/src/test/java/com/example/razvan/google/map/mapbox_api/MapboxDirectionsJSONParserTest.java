package com.example.razvan.google.map.mapbox_api;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MapboxDirectionsJSONParserTest {

    @Test
    public void parse() {
    }

    @Test
    public void parseGeoJson() {
    }

    @Test
    public void decodeGeometry() {
        String encoded = "_p~iF~ps|U_ulLnnqC_mqNvxq`@\",10";
        int[] testArray = {3850000, -12020000, 220000, -75000, 255200, -550300};
        int[] actualResponseArray  = {0, 0, 0, 0, 0, 0};
        int index = 0;
        List<Integer>integers = com.example.razvan.google.map.mapbox_api.MapboxDirectionsJSONParser.decodeGeometry(encoded);
        for (Integer integer : integers){
            actualResponseArray[index] = integer;
            ++index;
        }
        assertArrayEquals(testArray, actualResponseArray);
    }

    @Test
    public void decodePoly() {
        String encoded = "_p~iF~ps|U_ulLnnqC_mqNvxq`@";
        List<LatLng>latLngList = com.example.razvan.google.map.mapbox_api.MapboxDirectionsJSONParser.decodePoly(encoded);

        List<LatLng>testLatLngList = new ArrayList<>();
        testLatLngList.add(new LatLng(38.5, -120.2));
        testLatLngList.add(new LatLng(40.7, -120.95));
        testLatLngList.add(new LatLng(43.252, -126.453));

        for (int index = 0; index < latLngList.size(); ++index){
            assertEquals(latLngList.get(index), testLatLngList.get(index));
        }
    }
}
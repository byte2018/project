package com.example.razvan.google.auth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.test.espresso.PerformException;
import androidx.test.filters.LargeTest;
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.razvan.google.R;
import com.example.razvan.google.model.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class LogInActivityTest {

    @Rule
    public ActivityTestRule<LogInActivity>logInActivityTestRule = new ActivityTestRule<>(LogInActivity.class, true, false);

    private User user;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        user = new User(
                sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_API_TOKEN, null)

        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();

        logInActivityTestRule.launchActivity(new Intent());
    }

    @Test
    public void canActivityLunch() {
        onView(withId(R.id.log_in_view)).check(matches(isDisplayed()));
        onView(withId(R.id.e_mail_view)).check(matches(isDisplayed()));
        onView(withId(R.id.e_mail_id)).check(matches(isDisplayed()));
        onView(withId(R.id.password_view)).check(matches(isDisplayed()));
        onView(withId(R.id.password_id)).check(matches(isDisplayed()));
        onView(withId(R.id.remembetr_me)).check(matches(isDisplayed()));
        onView(withId(R.id.log_in_id)).check(matches(isDisplayed()));
    }

    @Test
    public void canUserLogIn() throws PerformException {
        onView(withId(R.id.e_mail_id)).perform(typeText("admin@admin.admin"));
        onView(withId(R.id.password_id)).perform(typeText("admin"));
        onView(withId(R.id.log_in_id)).perform(click());
    }

    @After
    public void tearDown() throws Exception {
        SharedPreferences.Editor editor = InstrumentationRegistry.getInstrumentation().getTargetContext().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString(SignInActivity.ACCOUNT_FIRST_NAME, user.getFirstName());
        editor.putString(SignInActivity.ACCOUNT_LAST_NAME, user.getLastName());
        editor.putString(SignInActivity.ACCOUNT_EMAIL, user.getEmail());
        editor.putString(SignInActivity.ACCOUNT_API_TOKEN, user.getApiToken());
        editor.apply();
    }
}
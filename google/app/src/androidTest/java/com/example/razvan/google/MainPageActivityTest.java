package com.example.razvan.google;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainPageActivityTest {

    @Rule
    public ActivityTestRule<MainPageActivity>mainPageActivityTestRule = new ActivityTestRule<>(MainPageActivity.class, true, false);

    private User user;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        user = new User(
                sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, null),
                sharedPreferences.getString(SignInActivity.ACCOUNT_API_TOKEN, null)

        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();

        mainPageActivityTestRule.launchActivity(new Intent());
    }

    @Test
    public void canActivityLunch() {
        onView(withId(R.id.sign_in)).check(matches(isDisplayed()));
    }

    @Test
    public void canGoToLoginPage() {
        onView(withId(R.id.log_in)).perform(click());
    }

    @Test
    public void canGoToSignInPage() {
        onView(withId(R.id.sign_in)).perform(click());
    }

    @After
    public void tearDown() throws Exception {
        SharedPreferences.Editor editor = InstrumentationRegistry.getInstrumentation().getTargetContext().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString(SignInActivity.ACCOUNT_FIRST_NAME, user.getFirstName());
        editor.putString(SignInActivity.ACCOUNT_LAST_NAME, user.getLastName());
        editor.putString(SignInActivity.ACCOUNT_EMAIL, user.getEmail());
        editor.putString(SignInActivity.ACCOUNT_API_TOKEN, user.getApiToken());
        editor.apply();
    }
}
package com.example.razvan.google.visited_monuments.mapbox_api;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.razvan.google.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class MapboxVisitedMonumentsActivityTest {

    @Rule
    public ActivityTestRule<MapboxVisitedMonumentsActivity> mapboxVisitedMonumentsActivityTestRule = new ActivityTestRule<>(MapboxVisitedMonumentsActivity.class);

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void canActivityLaunch() {
        //onView(withId(R.id.visited_monuments_map_view)).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
    }
}
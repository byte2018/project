package com.example.razvan.google.map.mapbox_api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapboxDirectionsJSONParser {

    public List<List<HashMap<String,String>>> parse(JSONObject jObject){

        List<List<HashMap<String,String>>> routes = new ArrayList<>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;

        try {
            jRoutes = jObject.getJSONArray("routes");

            for(int index = 0;index < jRoutes.length(); ++index) {
                String polyLine  = jRoutes.getJSONObject(index).getString("geometry");
                List<LatLng> list = decodePoly(polyLine);

                List path = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < list.size(); ++i){
                    HashMap<String, String>hashMap = new HashMap<>();
                    hashMap.put("lat", Double.toString(((LatLng)list.get(i)).getLatitude()) );
                    hashMap.put("lng", Double.toString(((LatLng)list.get(i)).getLongitude()) );
                    path.add(hashMap);
                }

                jLegs = ( (JSONObject)jRoutes.get(index)).getJSONArray("legs");

                for(int j=0;j<jLegs.length();j++){
                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");

                    for(int k=0;k<jSteps.length();k++){
                        String stepPolyLine = jSteps.getJSONObject(index).getString("geometry");
                        List<LatLng>stepPolyLineList = decodePoly(stepPolyLine);

                        for (int i = 0; i < stepPolyLineList.size(); ++i){
                            HashMap<String, String>hashMap = new HashMap<>();
                            hashMap.put("lat", Double.toString(((LatLng)stepPolyLineList.get(i)).getLatitude()) );
                            hashMap.put("lng", Double.toString(((LatLng)stepPolyLineList.get(i)).getLongitude()) );
                            path.add(hashMap);
                        }
                    }
                    routes.add(path);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
        }
        return routes;
    }

    @Nullable
    public ArrayList<LatLng> parseGeometryCoordinates(JSONObject jsonObject) {
        ArrayList<LatLng>coordinates = new ArrayList<>();
        try {
            JSONArray routes = jsonObject.getJSONArray("routes");
            JSONArray jsonCoordinates = routes.getJSONObject(0).getJSONObject("geometry").getJSONArray("coordinates");
            for (int index = 0; index <= jsonCoordinates.length(); ++index) {
                JSONArray location = jsonCoordinates.getJSONArray(index);
                coordinates.add(new LatLng(location.getDouble(0), location.getDouble(1)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return coordinates;
    }

    public ArrayList<LatLng> parseRoute (JSONObject jObject){
        ArrayList<LatLng> route = new ArrayList<>();

        try {
            JSONArray jsonRoutes = jObject.getJSONArray("routes");
            /*JSONObject jsonGeometry = jsonRoutes.getJSONObject(0).getJSONObject("geometry");
            if (jsonGeometry != null) {
                JSONArray coordinates = jsonGeometry.getJSONArray("coordinates");
                for (int index = 0; index < coordinates.length(); ++index) {
                    JSONArray coordinate = coordinates.getJSONArray(index);
                    LatLng point = new LatLng(coordinate.getDouble(1), coordinate.getDouble(0));
                    route.add(point);
                }
            }*/
            JSONArray jsonLegs = jsonRoutes.getJSONObject(0).getJSONArray("legs");
            for (int index = 0; index < jsonLegs.length(); ++index) {
                JSONArray jsonSteps = jsonLegs.getJSONObject(index).getJSONArray("steps");
                for (int secondIndex = 0; secondIndex < jsonSteps.length(); ++secondIndex) {
                    JSONArray jsonStepGeometryCoordinates = jsonSteps.getJSONObject(secondIndex).getJSONObject("geometry").getJSONArray("coordinates");
                    for (int thirdIndex = 0; thirdIndex < jsonStepGeometryCoordinates.length(); ++thirdIndex) {
                        JSONArray coordinate = jsonStepGeometryCoordinates.getJSONArray(thirdIndex);
                        LatLng point = new LatLng(coordinate.getDouble(1), coordinate.getDouble(0));
                        route.add(point);
                    }
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return route;
    }

    public static List<Integer> decodeGeometry(@NonNull String encodedPolylines) {
        List<Integer> trucks = new ArrayList<Integer>();
        int truck = 0;
        int carriage_q = 0;
        for (int x = 0, xx = encodedPolylines.length(); x < xx; ++x) {
            int i = encodedPolylines.charAt(x);
            i -= 63;
            int _5_bits = i << (32 - 5) >>> (32 - 5);
            truck |= _5_bits << carriage_q;
            carriage_q += 5;
            boolean is_last = (i & (1 << 5)) == 0;
            if (is_last) {
                boolean is_negative = (truck & 1) == 1;
                truck >>>= 1;
                if (is_negative) {
                    truck = ~truck;
                }
                trucks.add(truck);
                carriage_q = 0;
                truck = 0;
            }
        }
        return trucks;
    }


    public static List<LatLng> decodePoly(@NonNull String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index) - 63;
                ++index;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index) - 63;
                ++index;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

}

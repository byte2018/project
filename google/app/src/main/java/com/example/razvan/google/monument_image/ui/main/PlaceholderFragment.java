package com.example.razvan.google.monument_image.ui.main;

import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class PlaceholderFragment extends Fragment {

    protected static final String POSITION = "section_number";
    protected static final String IMAGE_PATH = "image_url";
    protected static final String TITLE = "title";
    protected static final String BASE_API_URL = "http://192.168.137.1:8000/monument-image/view-image/";

    private int position;
    private String imagePath;
    private String title;

    private PageViewModel pageViewModel;

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        assert getArguments() != null;
        this.position = getArguments().getInt(POSITION);
        this.imagePath = getArguments().getString(IMAGE_PATH);
        this.title = getArguments().getString(TITLE);
    }

    public static PlaceholderFragment newInstance(int position, String title, String imagePath) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        bundle.putString(TITLE, title);
        bundle.putString(IMAGE_PATH, imagePath);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(POSITION);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail, container, false);
        final ImageView imageView = root.findViewById(R.id.detailed_image);
        try {
            Glide.with(getContext())
                    .load(BASE_API_URL + Base64.encodeToString(URLEncoder.encode(imagePath, "UTF-8").getBytes(), Base64.DEFAULT))
                    .into(imageView);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return root;
    }
}
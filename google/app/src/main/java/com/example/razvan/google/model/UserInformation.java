package com.example.razvan.google.model;

public class UserInformation {

    private int id;
    private String imagePath;
    private int userId;

    public UserInformation(int id, String imagePath, int userId) {
        this.id = id;
        this.imagePath = imagePath;
        this.userId = userId;
    }

    public UserInformation(String imagePath, int userId) {
        this.imagePath = imagePath;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

package com.example.razvan.google.monument;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.Comment;
import com.example.razvan.google.model.MonumentImage;
import com.example.razvan.google.monument_image.MonumentImageActivity;
import com.example.razvan.google.visited_monuments.google_api.VisitedMonumentsActivity;

import org.jetbrains.annotations.Contract;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class MonumentActivity extends AppCompatActivity {

    public static final String API_POST_URL = "http://192.168.137.1:8000/feedback/add";
    public static final String API_RATING_URL = "http://192.168.137.1:8000/rating/add";
    public static final String VISITED_MONUMENT_QUERY_URL = "http://192.168.137.1:8000/visited-monuments/get-status";
    public static final String VISITED_MONUMENT_ADD_QUERY_URL = "http://192.168.137.1:8000/visited-monuments/add";
    public static final int URL_RESULT_OK = 200;
    public String USER_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String MONUMENTS_QUERY = "http://192.168.137.1:8000/monument/get/";
    public String COMMENT_URL = "http://192.168.137.1:8000/feedback/get-all/";
    public String RATING_URL = "http://192.168.137.1:8000/rating/view-rating/";
    public String MONUMENT_RATING_NUMBER_UPDATE = "http://192.168.137.1:8000/rating-numbers/update-increment/";
    public String AVERAGE_MONUMENT_RATING_URL = "http://192.168.137.1:8000/rating/average-rating/";
    public String MONUMENT_WIKI_IMAGE_URL = "http://192.168.137.1:8000/monument/get-image/";
    public String MONUMENT_IMAGES_URL = "http://192.168.137.1:8000/monument-image/view-few-images/";
    public String USER_IMAGE_PATH_URL = "http://192.168.137.1:8000/user-information/get-image-path/";

    private RatingBar averageRating;
    private RatingBar userRatingView;
    private TextView monumentRatingView;
    private TextView userRatingNumberView;
    private FloatingActionButton rateViewFab;
    private FloatingActionButton sendComment;
    private CardView wikiLinkView;
    private Button monumentAllImagesButton;
    private Button visitMonumentButton;
    private EditText userCommentView;
    private ImageView headerImageView;

    public RecyclerView commentRecyclerView;
    public MonumentCommentAdapter monumentCommentAdapter;
    public List<Comment>commentList = new ArrayList<>();

    private RecyclerView monumentImageRecyclerView;
    private MonumentImageAdapter monumentImageAdapter;
    private ArrayList<MonumentImage>monumentImageArrayList = new ArrayList<>();

    public String monumentName;
    public float rating;
    public float userRating;
    public double monumentRating;
    public String userComment;
    public int userId;
    public int monumentId;
    public String wikiLink;
    public double averageRatingFromServer;
    public String monumentHeaderImage;
    private boolean monumentVisitedStatus;
    private String userImagePath;

    public HttpClient httpClient = new DefaultHttpClient();
    public HttpResponse httpResponse;
    public HttpPost httpPost = new HttpPost(API_POST_URL);
    public String ticketListFromAllMonuments;

    public List<NameValuePair>nameValuePairs = new ArrayList<>();

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_monument);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setUpFields();
        commentRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        commentRecyclerView.setLayoutManager(linearLayoutManager);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        monumentImageRecyclerView.setLayoutManager(layoutManager);

        monumentName = getIntent().getStringExtra("monumentName");
        toolbar.setTitle(monumentName);
        ticketListFromAllMonuments = getIntent().getStringExtra(VisitedMonumentsActivity.TICKET_LIST);

        //Intent intent = new Intent(MonumentActivity.this, VisitedMonumentsActivity.class);
        //intent.putExtra("monumentName", monumentName);

        getUserId();
        String urlEncodedMonumentName = null;
        try {
            urlEncodedMonumentName = urlEncodeString(monumentName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

       /* try {
            String base64UrlEncodedMonumentName = Base64.encodeToString(URLEncoder.encode(monumentName, "UTF-8").getBytes(), Base64.DEFAULT);
            MONUMENTS_QUERY += base64UrlEncodedMonumentName.substring(0, base64UrlEncodedMonumentName.length() - 1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
        MONUMENTS_QUERY += urlEncodedMonumentName;
        COMMENT_URL += urlEncodedMonumentName;
        RATING_URL = RATING_URL + userId + "/" + urlEncodedMonumentName;
        MONUMENT_RATING_NUMBER_UPDATE += urlEncodedMonumentName;
        AVERAGE_MONUMENT_RATING_URL += urlEncodedMonumentName;

        runMonumentQoery();
        runCommentQuery(COMMENT_URL);
        runRatingQuery(RATING_URL);
        getAverageRatingFromServer(AVERAGE_MONUMENT_RATING_URL);

        MONUMENT_WIKI_IMAGE_URL += monumentId;
        monumentHeaderImage = getImagePathFromDatabase(MONUMENT_WIKI_IMAGE_URL);
        if (monumentHeaderImage != null) {
            Glide.with(this)
                    .load(monumentHeaderImage)
                    .fitCenter()
                    .into(headerImageView);
        }

        MONUMENT_IMAGES_URL += monumentId;
        monumentImageArrayList = getMonumentImagesFromDatabase(MONUMENT_IMAGES_URL);
        monumentImageAdapter = new MonumentImageAdapter(this, monumentImageArrayList);
        monumentImageRecyclerView.setAdapter(monumentImageAdapter);

        monumentRatingView.setText(String.valueOf(averageRatingFromServer));
        averageRating.setRating((float) averageRatingFromServer);

        monumentCommentAdapter = new MonumentCommentAdapter(this, commentList, userId);
        commentRecyclerView.setAdapter(monumentCommentAdapter);

        userRatingNumberView.setText(String.valueOf(userRating));
        userRatingView.setRating(userRating);

        USER_IMAGE_PATH_URL += userId;
        userImagePath = getUserImagePathFromDatabase(USER_IMAGE_PATH_URL);

        goToWebLink(wikiLink);
        postComment();

        rateViewFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRatingDialog();
            }
        });

        monumentAllImagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MonumentActivity.this, MonumentImageActivity.class);
                intent.putExtra("monument_id", monumentId);
                intent.putExtra("user_id", userId);
                startActivity(intent);
            }
        });

        monumentVisitedStatus = getMonumentVisitedStatus(VISITED_MONUMENT_QUERY_URL);
        if (monumentVisitedStatus) {
            visitMonumentButton.setText("Visited");
        } else {
            visitMonumentButton.setText("Visit");
        }

        visitMonumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addVisitedMonument(VISITED_MONUMENT_ADD_QUERY_URL);
            }
        });
    }

    public String urlEncodeString(String stringToEncode) throws UnsupportedEncodingException {
        return URLEncoder.encode(stringToEncode, "UTF-8");
    }

    private void runRatingQuery(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse;

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                assert jsonArray != null;
                userRating = (float) jsonArray.getJSONObject(0).getDouble("rating");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            httpGet.releaseConnection();
        }
    }

    private String getUserImagePathFromDatabase(String API_URL) {
        JSONObject jsonObject = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    jsonObject = convertStreamToJsonObject(inputStream);
                    inputStream.close();
                }
            } else {
                Toast.makeText(getBaseContext(), "Error trying to get profile image:" + httpResponse.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        if (jsonObject != null && jsonObject.has("image_path")) {
            try {
                return jsonObject.getString("image_path");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "null";
    }

    public void postComment() {
        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    userComment = userCommentView.getText().toString();
                    if (!userComment.equals("")) {
                        saveFields();
                        userCommentView.clearFocus();
                        userCommentView.setText(null);
                    }else {
                        Toast.makeText(getBaseContext(), "You didn't type anything", Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void runCommentQuery(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                assert jsonArray != null;
                for (int index = 0; index < jsonArray.length(); ++index){
                    JSONObject jsonObject = jsonArray.getJSONObject(index);
                    commentList.add(new Comment(
                            jsonObject.getInt("comment_id"),
                            jsonObject.getInt("id"),
                            jsonObject.getString("user_first_name") + " " + jsonObject.getString("user_last_name"),
                            jsonObject.getString("comment"),
                            jsonObject.getString("image_path"),
                            this
                    ));
                }
                //userImagePath = jsonArray.getJSONObject(0).getString("image_path");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }
    }

    @Contract(pure = true)
    private boolean getMonumentVisitedStatus(final String API_URL) {
        boolean isMonumentVisited = false;
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        httpGet.addHeader("user_id", String.valueOf(userId));
        httpGet.addHeader("monument_id", String.valueOf(monumentId));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();;
                    isMonumentVisited = Objects.requireNonNull(convertStreamToJsonObject(inputStream)).getBoolean("status");
                    inputStream.close();
                }
            } else {
                Toast.makeText(getBaseContext(), "Error:" + httpResponse.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }
        return isMonumentVisited;
    }

    @Nullable
    public String getImagePathFromDatabase(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        String imagePath = null;
        try {
            httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    imagePath = convertStreamToJsonObject(inputStream).getString("image_path");
                    inputStream.close();
                }
            } else {
                Toast.makeText(getBaseContext(), "Error" + httpResponse.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }
        return imagePath;
    }

    public void openRatingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MonumentActivity.this);
        View ratingUI = getLayoutInflater().inflate(R.layout.pop_window_rate_monument,null);

        builder.setView(ratingUI);
        final AlertDialog alertDialog = builder.create();

        final RatingBar ratingBar = (RatingBar)ratingUI.findViewById(R.id.rating_bar_id);
        ratingBar.setRating(userRating);
        FloatingActionButton done = (FloatingActionButton)ratingUI.findViewById(R.id.done_rating_button_id);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rating = ratingBar.getRating();
                postDataToServer(API_RATING_URL);
                if (userRating == 0.0f) {
                    updateNumberOfRatings(MONUMENT_RATING_NUMBER_UPDATE);
                }
                getAverageRatingFromServer(AVERAGE_MONUMENT_RATING_URL);
                monumentRatingView.setText(String.valueOf(averageRatingFromServer));
                averageRating.setRating((float) averageRatingFromServer);
                userRatingNumberView.setText(String.valueOf(rating));
                userRatingView.setRating(rating);
                userRating = rating;
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void updateNumberOfRatings (String API_URL){
        HttpPut httpPut = new HttpPut(API_URL);
        httpPut.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpClient httpClient = new DefaultHttpClient();

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpPut);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK) {
                Toast.makeText(getBaseContext(), "incremented", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private UrlEncodedFormEntity createHttpPostEntity () throws UnsupportedEncodingException {
        List<BasicNameValuePair> nameValuePairs = new ArrayList<>();

        nameValuePairs.add(new BasicNameValuePair("rating", String.valueOf(rating)));
        nameValuePairs.add(new BasicNameValuePair("user_id", Integer.toString(userId)));
        nameValuePairs.add(new BasicNameValuePair("monument_id", Integer.toString(monumentId)));

        return new UrlEncodedFormEntity(nameValuePairs);
    }

    private void postDataToServer(String API_URL) {
        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        HttpPost httpPost = new HttpPost(API_URL);
        httpPost.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        try {
            httpPost.setEntity(createHttpPostEntity());
            httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK){
                Toast.makeText(this, "Rating saved successful", Toast.LENGTH_SHORT).show();
                userRatingView.setRating(rating);
            }else {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            httpPost.releaseConnection();
        }
    }

    public void goToWebLink (final String link){
        wikiLinkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!link.equals("0")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(intent);
                }else {
                    Toast.makeText(getBaseContext(), "No external reference", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setUpFields(){
        rateViewFab = (FloatingActionButton)findViewById(R.id.rate_fab);
        wikiLinkView = (CardView)findViewById(R.id.wiki_link_id);
        monumentAllImagesButton = (Button)findViewById(R.id.see_all_images_button);
        visitMonumentButton = (Button)findViewById(R.id.visit_monument_button);
        monumentImageRecyclerView = (RecyclerView)findViewById(R.id.few_images_recycler_view);
        monumentRatingView = (TextView)findViewById(R.id.rating_number);
        averageRating = (RatingBar)findViewById(R.id.monument_average_rating);
        commentRecyclerView = (RecyclerView)findViewById(R.id.comment_recycler_view);
        sendComment = (FloatingActionButton)findViewById(R.id.send_comment);
        userCommentView = (EditText)findViewById(R.id.comment_edit_text);
        userRatingView = (RatingBar)findViewById(R.id.user_monument_rating);
        userRatingNumberView = (TextView)findViewById(R.id.user_rating_number);
        headerImageView = (ImageView)findViewById(R.id.header_image_view);
    }

    public void getAverageRatingFromServer (String API_URL){
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        HttpClient httpClient = new DefaultHttpClient();

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONObject jsonObject = convertStreamToJsonObject(inputStream);
                assert jsonObject != null;
                averageRatingFromServer = jsonObject.getDouble("average_rating");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            httpGet.releaseConnection();
        }
    }

    @SuppressLint("SetTextI18n")
    private void addVisitedMonument(String API_URL) {
        HttpPost httpPost = new HttpPost(API_URL);
        httpPost.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        List<NameValuePair>nameValuePairs = new ArrayList<>(2);
        nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(userId)));
        nameValuePairs.add(new BasicNameValuePair("monument_id", String.valueOf(monumentId)));

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() == URL_RESULT_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    if (Objects.requireNonNull(convertStreamToJsonObject(inputStream)).has("deleted")){
                        visitMonumentButton.setText("Visit");
                    } else {
                        visitMonumentButton.setText("Visited");
                    }
                    inputStream.close();
                }
            } else {
                Toast.makeText(getBaseContext(), "Error:" + httpResponse.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpPost.releaseConnection();
        }

    }

    public void runMonumentQoery() {
        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        HttpGet httpGet = new HttpGet(MONUMENTS_QUERY);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                assert jsonArray != null;
                monumentId = jsonArray.getJSONObject(0).getInt("id");
                wikiLink = jsonArray.getJSONObject(0).getString("link_wiki");
                wikiLink = "https://" + wikiLink;
                monumentRating = jsonArray.getJSONObject(0).getDouble("rating");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveFields() throws UnsupportedEncodingException {
        nameValuePairs.add(new BasicNameValuePair("comment", userComment));
        nameValuePairs.add(new BasicNameValuePair("user_id", Integer.toString(userId)));
        nameValuePairs.add(new BasicNameValuePair("monument_id", Integer.toString(monumentId)));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        httpPost.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse httpResponse;
        try {
            httpResponse = httpClient.execute(httpPost);
            System.out.println(httpResponse.getStatusLine().getStatusCode());
            if (httpResponse.getStatusLine().getStatusCode() == 200){
                HttpEntity httpEntity = httpResponse.getEntity();
                assert httpEntity != null;
                InputStream inputStream = httpEntity.getContent();
                int commentId = Objects.requireNonNull(convertStreamToJsonObject(inputStream)).getInt("id");
                SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
                Toast.makeText(getBaseContext(), "Comment Saved", Toast.LENGTH_SHORT).show();
                commentList.add(0, new Comment(
                        commentId,
                        userId,
                        sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, ""),
                        userComment,
                        userImagePath,
                        this
                ));
                monumentCommentAdapter.notifyDataSetChanged();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public ArrayList<MonumentImage>getMonumentImagesFromDatabase(String API_URL) {
        ArrayList<MonumentImage>monumentImageArrayList = new ArrayList<>();
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                    assert jsonArray != null;
                    for (int index = 0; index < jsonArray.length(); ++index) {
                        JSONObject jsonObject = jsonArray.getJSONObject(index);
                        monumentImageArrayList.add(new MonumentImage(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("monument_id"),
                                jsonObject.getInt("user_id")
                        ));
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return monumentImageArrayList;
    }

    @NonNull
    public static Bitmap convertStringToBitmap (@NonNull String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    @Nullable
    private static JSONObject convertStreamToJsonObject (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getUserId() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        String userEmail = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "");
        byte[] userEmailToEncode = userEmail.getBytes();
        String base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        USER_API_URL = USER_API_URL + base64EncodedEmail;

        HttpGet httpGet = new HttpGet(USER_API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONObject userJSON = convertStreamToJsonObject(inputStream);
                assert userJSON != null;
                userId = userJSON.getInt("id");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

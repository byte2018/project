package com.example.razvan.google.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.UserInformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

public class StatisticsProfileActivity extends Fragment {

    private static final int GET_FROM_GALLERY = 1;
    private static final int RESULT_URL_OK = 200;
    private static final String IMAGE_POST_URL = "http://192.168.137.1:8000/user-information/add";
    public String USER_INFORMATION_URL = "http://192.168.137.1:8000/user-information/view-information/";
    public String USER_INFORMATION = "http://192.168.137.1:8000/user-information/get-image-path/";
    public String VIEW_IMAGE_URL = "http://192.168.137.1:8000/user-information/view-image/";
    public String USERS_API_URL = "http://192.168.137.1:8000/user/get-email/";

    public FloatingActionButton uploadImage;
    public CircleImageView profileImage;
    public TextView userNameView;

    public int userId;

    public Bitmap bitmapImage;
    public String imageInStringFormatFromJson;
    public Bitmap bitmapImageFromJson;

    private HttpClient httpClient = new DefaultHttpClient();
    private HttpResponse httpResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statitics_page, container, false);

        uploadImage = view.findViewById(R.id.floatingActionButton);
        profileImage = view.findViewById(R.id.imageView1);
        userNameView = view.findViewById(R.id.user_name_view_id);

        generateUrl();
        runQuery();

        USER_INFORMATION_URL += userId;
        USER_INFORMATION += userId;
        getUserInformationFromDatabase(USER_INFORMATION, profileImage);
        //getUserInformation(USER_INFORMATION_URL);

        setUserName();
        selectImageFromGallery();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();

            String[] filePath = { MediaStore.Images.Media.DATA };
            assert selectedImage != null;
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            File image = new File(imagePath);

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;

            bitmapImage = BitmapFactory.decodeFile(imagePath, bitmapOptions);

            cursor.close();

            uploadImageToServer(IMAGE_POST_URL, image, profileImage);
            //postImageOnDatabase();

            //profileImage.setImageBitmap(null);
            //profileImage.setImageBitmap(bitmapImage);

            //setActionBarImage(image);
        }
    }

    public void setActionBarImage(File image) {
        NavigationView navigationView = (NavigationView)getActivity().findViewById(R.id.nav_view);
        CircleImageView profileImageView = (CircleImageView)navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        Glide.with(getActivity())
                .load(image)
                .into(profileImageView);
    }

    public void uploadImageToServer(final String API_URL, final File image, final CircleImageView circleImageView) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String contentType = getFileType(image.getPath());
                String filePath = image.getAbsolutePath();

                OkHttpClient okHttpClient = new OkHttpClient();

                RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), image);
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", contentType)
                        .addFormDataPart("image", filePath.substring(filePath.lastIndexOf("/")+1), fileBody)
                        .addFormDataPart("user_id", String.valueOf(userId))
                        .build();

                Request request = new Request.Builder()
                        .url(API_URL)
                        .addHeader("api_token", Objects.requireNonNull(getActivity())
                                .getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE)
                                .getString(SignInActivity.ACCOUNT_API_TOKEN, ""))
                        .post(requestBody)
                        .build();

                try {
                    Response response = okHttpClient.newCall(request).execute();
                    if (response.isSuccessful()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                Glide.with(getActivity())
                                    .load(image)
                                    .into(circleImageView);
                                setActionBarImage(image);
                            }
                        });
                    }else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Error:" + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private String getFileType(String filePath) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    private void selectImageFromGallery() {
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setUserName() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        userNameView.setText(
                sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
    }

    @NonNull
    public static String convertImageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @NonNull
    public static Bitmap convertStringToBitmap (String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    @NonNull
    public UrlEncodedFormEntity createFormEntity() {
        List<NameValuePair> nameValuePairArrayList = new ArrayList<NameValuePair>();
        nameValuePairArrayList.add(new BasicNameValuePair("image", convertImageToString(bitmapImage)));
        nameValuePairArrayList.add(new BasicNameValuePair("user_id", String.valueOf(userId)));
        try {
            return new UrlEncodedFormEntity(nameValuePairArrayList);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    private static JSONArray convertStreamToJsonArray(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void postImageOnDatabase() {
        HttpPost httpPost = new HttpPost(IMAGE_POST_URL);
        httpPost.setHeader("api_token", getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpResponse httpResponse;
        HttpClient httpClient = new DefaultHttpClient();

        httpPost.setEntity(createFormEntity());

        try {
            httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() == RESULT_URL_OK){
                Toast.makeText(getContext(), "Image Posted", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getUserInformationFromDatabase(String API_URL, @NonNull CircleImageView circleImageView) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (userInformation != null) {
            try {
                Glide.with(getActivity())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(circleImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public void getUserInformation(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                inputStream.close();
                imageInStringFormatFromJson = jsonArray.getJSONObject(0).getString("image");
                bitmapImageFromJson = convertStringToBitmap(imageInStringFormatFromJson);
                if (bitmapImageFromJson != null) {
                    profileImage.setImageBitmap(null);
                    profileImage.setImageBitmap(bitmapImageFromJson);
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void runQuery() {
        HttpGet httpGet = new HttpGet(USERS_API_URL);
        httpGet.setHeader("api_token", Objects.requireNonNull(getContext()).getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE)
                .getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                assert jsonObject != null;
                userId = jsonObject.getInt("id");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void generateUrl (){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        byte[] userEmailToEncode;
        String base64EncodedEmail;
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0, base64EncodedEmail.length() - 1);
        USERS_API_URL = USERS_API_URL + base64EncodedEmail;
    }

}

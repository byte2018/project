package com.example.razvan.google.monument_image;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClicked);
}

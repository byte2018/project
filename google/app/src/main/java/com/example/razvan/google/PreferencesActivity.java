package com.example.razvan.google;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;
import com.example.razvan.google.model.UserInformation;
import com.example.razvan.google.profile.ProfileActivity;
import com.example.razvan.google.visited_monuments.mapbox_api.MapboxVisitedMonumentsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.hdodenhof.circleimageview.CircleImageView;

public class PreferencesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int STATUS_CODE_OK = 200;
    public String API_URL = "http://192.168.137.1:8000/preferences/update/";
    public String USER_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String PREFERENCES_USER_URL = "http://192.168.137.1:8000/preferences/view-user/";
    public String USER_INFORMATION = "http://192.168.137.1:8000/user-information/get-image-path/";
    public String VIEW_IMAGE_URL = "http://192.168.137.1:8000/user-information/view-image/";

    public Switch archaeologySwitch;
    public Switch architectureSwitch;
    public Switch memorialsSwitch;
    public Switch statuesSwitch;
    public EditText toleranceEditText;
    public Button saveButton;
    public Spinner travelModeView;

    public SharedPreferences sharedPreferences;
    public String travelMode;

    private HttpClient httpClient = new DefaultHttpClient();
    private HttpResponse httpResponse;

    public HashMap<String, Integer>preferences = new HashMap<String, Integer>();
    public List<NameValuePair>httpPostEntity = new ArrayList<>();
    public int userId;
    public int preferencesId;

    public float tolerance = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        getUserId();
        PREFERENCES_USER_URL += userId;
        USER_INFORMATION += userId;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setNavigationHeaderTitles(getUserInformationFromDatabase(USER_INFORMATION));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        runPreferencesUrl();
        API_URL += preferencesId;

        sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);

        setupFields();
        initSwitch();
        setTravelMode();

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.travel_mode_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        travelModeView.setAdapter(arrayAdapter);
        String defaultTravelMode = sharedPreferences.getString(MapActivity.TRAVEL_MODE, "");
        travelModeView.setSelection(arrayAdapter.getPosition(defaultTravelMode));

        updatePreferences();

        loadLocale();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveMapPreferances();
                saveTravelMode();
                try {
                    savePreferences();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getBaseContext(), "Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeaderTitles(UserInformation userInformation) {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        TextView userNameView = (TextView)headerView.findViewById(R.id.user_name_nav);
        userNameView.setText(
                sharedPreferences.getString(
                        SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
        TextView userEmailView = (TextView)headerView.findViewById(R.id.user_email_nav);
        userEmailView.setText(sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, ""));
        CircleImageView profileImageView = (CircleImageView)headerView.findViewById(R.id.profile_image);

        if (userInformation != null) {
            try {
                Glide.with(getBaseContext())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(profileImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @NonNull
    public static Bitmap convertStringToBitmap (String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    public void runPreferencesUrl() {
        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);
        HttpGet httpGet = new HttpGet(PREFERENCES_USER_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray preferencesJSONArray = convertStreamToJsonArray(inputStream);
                assert preferencesJSONArray != null;
                JSONObject preferencesJSON = preferencesJSONArray.getJSONObject(0);
                preferencesId = preferencesJSON.getInt("id");
                initPreferencesHashMap(preferencesJSON);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void initPreferencesHashMap(JSONObject jsonObject) throws JSONException {
        if (jsonObject != null){
            preferences.put("archaeology", jsonObject.getInt("archaeology"));
            preferences.put("architecture", jsonObject.getInt("architecture"));
            preferences.put("memorials", jsonObject.getInt("memorials"));
            preferences.put("statues", jsonObject.getInt("statues"));
        }else {
            preferences.put("archaeology", 0);
            preferences.put("architecture", 0);
            preferences.put("memorials", 0);
            preferences.put("statues", 0);
        }
    }

    private void initSwitch() {
        archaeologySwitch.setChecked(preferences.get("archaeology").equals(1));
        architectureSwitch.setChecked(preferences.get("architecture").equals(1));
        memorialsSwitch.setChecked(preferences.get("memorials").equals(1));
        statuesSwitch.setChecked(preferences.get("statues").equals(1));
    }

    public void getUserId() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        String userEmail = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "");
        byte[] userEmailToEncode = userEmail.getBytes();
        String base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        USER_API_URL = USER_API_URL + base64EncodedEmail;

        HttpGet httpGet = new HttpGet(USER_API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONObject userJSON = convertStreamToJsonObject(inputStream);
                assert userJSON != null;
                userId = userJSON.getInt("id");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public UserInformation getUserInformationFromDatabase(String API_URL) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == STATUS_CODE_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonObject(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        return userInformation;
    }

    @Nullable
    private static JSONObject convertStreamToJsonObject (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void savePreferences() throws UnsupportedEncodingException {
        HttpPut httpPut = new HttpPut(API_URL);
        httpPut.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        httpPostEntity.add(new BasicNameValuePair("archaeology" ,preferences.get("archaeology").toString()));
        httpPostEntity.add(new BasicNameValuePair("architecture", preferences.get("architecture").toString()));
        httpPostEntity.add(new BasicNameValuePair("memorials", preferences.get("memorials").toString()));
        httpPostEntity.add(new BasicNameValuePair("statues", preferences.get("statues").toString()));
        httpPostEntity.add(new BasicNameValuePair("user_id", Integer.toString(userId)));

        httpPut.setEntity(new UrlEncodedFormEntity(httpPostEntity));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        HttpClient httpClient = new DefaultHttpClient();

        try {
            httpResponse = httpClient.execute(httpPut);
            if (httpResponse.getStatusLine().getStatusCode() == STATUS_CODE_OK){
                Toast.makeText(this, "Succes", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSwitchChanged(final Switch preferencesSwitch, final String key) {
        preferencesSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    preferences.put(key, 1);
                }else {
                    preferences.put(key, 0);
                }
            }
        });
    }

    private void updatePreferences() {
        onSwitchChanged(archaeologySwitch, "archaeology");
        onSwitchChanged(architectureSwitch, "architecture");
        onSwitchChanged(memorialsSwitch, "memorials");
        onSwitchChanged(statuesSwitch, "statues");
    }

    public void saveTravelMode() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MapActivity.TRAVEL_MODE, travelMode);
        editor.apply();
    }

    public void setTravelMode() {
        travelModeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                travelMode = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void saveMapPreferances() {
        String toleraceTex = toleranceEditText.getText().toString();
        if (!toleraceTex.equals("")) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putFloat(MapActivity.ROAD_TOLERANCE, Float.parseFloat(toleraceTex));
            editor.apply();
        }
    }

    public void setupFields() {
        archaeologySwitch = (Switch)findViewById(R.id.switch_archaeology);
        architectureSwitch = (Switch)findViewById(R.id.switch_architecture);
        memorialsSwitch = (Switch)findViewById(R.id.memorials_switch);
        statuesSwitch = (Switch)findViewById(R.id.statues_switch);
        toleranceEditText = (EditText)findViewById(R.id.tolerance);
        showRoadTolerace(toleranceEditText);
        saveButton = (Button)findViewById(R.id.save);
        travelModeView = (Spinner)findViewById(R.id.travel_mode);
    }

    private void showRoadTolerace(@NonNull EditText editText) {
        float roadTolerance = sharedPreferences.getFloat(MapActivity.ROAD_TOLERANCE, 0.0f);
        editText.setText(String.valueOf(roadTolerance));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            logOut();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_map) {
            startActivity(new Intent(PreferencesActivity.this, MapboxMapActivity.class));
        } else if (id == R.id.nav_account) {
            startActivity(new Intent(PreferencesActivity.this, ProfileActivity.class));
        } else if (id == R.id.nav_preferences) {
        } /*else if (id == R.id.nav_qr_scanner) {
            startActivity(new Intent(PreferencesActivity.this, QrScannerActivity.class));
        } */else if (id == R.id.nav_visited_monuments) {
            startActivity(new Intent(PreferencesActivity.this, MapboxVisitedMonumentsActivity.class));
        } else if (id == R.id.nav_language) {
            showChangeLanguageDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();
        sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(MapActivity.ROAD_TOLERANCE);
        editor.remove(MapActivity.TRAVEL_MODE);
        editor.apply();
        Intent change = new Intent(PreferencesActivity.this, MainPageActivity.class);
        startActivity(change);
    }

    public void showChangeLanguageDialog() {
        final String[] listItems = {"English", "Roumanian", "Hindi", "Russian", "Mandarin", "French", "Hungarian", "Greek", "German"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);
        builder.setTitle("Select language");
        builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        setLocale("en");
                        recreate();
                        break;
                    case 1:
                        setLocale("ro");
                        recreate();
                        break;
                    case 2:
                        setLocale("hi");
                        recreate();
                        break;
                    case 3:
                        setLocale("ru");
                        recreate();
                        break;
                    case 4:
                        setLocale("zh");
                        recreate();
                        break;
                    case 5:
                        setLocale("fr");
                        recreate();
                        break;
                    case 6:
                        setLocale("hu");
                        recreate();
                        break;
                    case 7:
                        setLocale("el");
                        recreate();
                        break;
                    case 8:
                        setLocale("de");
                        recreate();
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

}

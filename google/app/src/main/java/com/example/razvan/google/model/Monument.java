package com.example.razvan.google.model;

public class Monument {

    private int id;
    private String monumentName;
    private String monumentImageUrl;
    private String wikiLink;
    private float averageRating;
    private Location monumentLocation;
    private boolean monumentVisitedStatus;

    public Monument(int id, String monumentName, String monumentImageUrl, String wikiLink, float averageRating, Location monumentLocation, boolean monumentVisitedStatus) {
        this.id = id;
        this.monumentName = monumentName;
        this.monumentImageUrl = monumentImageUrl;
        this.wikiLink = wikiLink;
        this.averageRating = averageRating;
        this.monumentLocation = monumentLocation;
        this.monumentVisitedStatus = monumentVisitedStatus;
    }

    public Monument(String monumentName, String monumentImageUrl, String wikiLink, float averageRating, Location monumentLocation, boolean monumentVisitedStatus) {
        this.monumentName = monumentName;
        this.monumentImageUrl = monumentImageUrl;
        this.wikiLink = wikiLink;
        this.averageRating = averageRating;
        this.monumentLocation = monumentLocation;
        this.monumentVisitedStatus = monumentVisitedStatus;
    }

    public Monument(String monumentName, String monumentImageUrl, String wikiLink, float averageRating, Location monumentLocation) {
        this.monumentName = monumentName;
        this.monumentImageUrl = monumentImageUrl;
        this.wikiLink = wikiLink;
        this.averageRating = averageRating;
        this.monumentLocation = monumentLocation;
    }

    public Monument(int id, String monumentName, String monumentImageUrl, float averageRating, Location monumentLocation, boolean monumentVisitedStatus) {
        this.id = id;
        this.monumentName = monumentName;
        this.monumentImageUrl = monumentImageUrl;
        this.averageRating = averageRating;
        this.monumentLocation = monumentLocation;
        this.monumentVisitedStatus = monumentVisitedStatus;
    }

    public Monument(String monumentName, String monumentImageUrl, float averageRating, Location monumentLocation, boolean monumentVisitedStatus) {
        this.monumentName = monumentName;
        this.monumentImageUrl = monumentImageUrl;
        this.averageRating = averageRating;
        this.monumentLocation = monumentLocation;
        this.monumentVisitedStatus = monumentVisitedStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMonumentName() {
        return monumentName;
    }

    public void setMonumentName(String monumentName) {
        this.monumentName = monumentName;
    }

    public String getMonumentImageUrl() {
        return monumentImageUrl;
    }

    public void setMonumentImageUrl(String monumentImageUrl) {
        this.monumentImageUrl = monumentImageUrl;
    }

    public String getWikiLink() {
        return wikiLink;
    }

    public void setWikiLink(String wikiLink) {
        this.wikiLink = wikiLink;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }

    public Location getMonumentLocation() {
        return monumentLocation;
    }

    public void setMonumentLocation(Location monumentLocation) {
        this.monumentLocation = monumentLocation;
    }

    public boolean isMonumentVisitedStatus() {
        return monumentVisitedStatus;
    }

    public void setMonumentVisitedStatus(boolean monumentVisitedStatus) {
        this.monumentVisitedStatus = monumentVisitedStatus;
    }
}

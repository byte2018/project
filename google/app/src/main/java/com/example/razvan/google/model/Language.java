package com.example.razvan.google.model;

public class Language {

    public String languageName;
    public String locale;

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

}

package com.example.razvan.google.map.mapbox_api;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.MainPageActivity;
import com.example.razvan.google.PreferencesActivity;
import com.example.razvan.google.QrScannerActivity;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.model.Monument;
import com.example.razvan.google.model.UserInformation;
import com.example.razvan.google.monument.MonumentActivity;
import com.example.razvan.google.profile.ProfileActivity;
import com.example.razvan.google.visited_monuments.mapbox_api.MapboxVisitedMonumentsActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class MapboxMapActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, LocationEngineListener, PermissionsListener, GoogleApiClient.OnConnectionFailedListener {

    private static final float DEFAULT_ZOOM = 15.0f;
    private static final float INIT_ZOOM = 5.0f;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int MAX_RESULTS = 1;
    private static final int HTTP_RESPONSE_OK = 200;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new com.google.android.gms.maps.model.LatLng(-40, -168), new com.google.android.gms.maps.model.LatLng(71, 136));
    private static final float CONVERSION_CONSTANT = 1000;
    private double TOLERANCE = 10000;
    public String USERS_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String USER_INFORMATION = "http://192.168.137.1:8000/user-information/get-image-path/";
    public String VIEW_IMAGE_URL = "http://192.168.137.1:8000/user-information/view-image/";
    public String MONUMENTS_BY_PREFERENCES_URL = "http://192.168.137.1:8000/monument/get-by-preferences";
    public String MAPBOX_DIRECTIONS_BASE_URL = "https://api.mapbox.com/directions/v5/mapbox/";

    public MapView mapView;
    public MapboxMap map;

    private TextView noMonumentsTextView;
    private RecyclerView monumentListRecylerView;

    public PermissionsManager permissionsManager;
    public LocationEngine locationEngine;
    public LocationLayerPlugin locationLayerPlugin;

    public Location currentLocation;

    public HttpClient httpClient = new DefaultHttpClient();
    public HttpResponse httpResponse;

    public int userId;
    public JSONArray monumentsByPreferencesQueryResponse = new JSONArray();
    public ArrayList<LatLng>basePolyLine = new ArrayList<>();
    public ArrayList<LatLng>primaryPolyline = new ArrayList<>();
    public ArrayList<LatLng>wayPoints = new ArrayList<>();

    private MonumentListAdapter monumentListAdapter;
    private ArrayList<Monument>monumentArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_mapbox_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mapView = (MapView)findViewById(R.id.map_view_mapbox);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this::onMapReady);

        generateUrl();
        runUserQuery();
        USER_INFORMATION += userId;

        setNavigationHeaderTitles(getUserInformationFromDatabase(USER_INFORMATION));

        SharedPreferences sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        float sharedTolerance = sharedPreferences.getFloat(MapActivity.ROAD_TOLERANCE, 0.0f);
        if (sharedTolerance != 0){
            TOLERANCE = sharedTolerance * CONVERSION_CONSTANT;
        }

        FloatingActionButton floatingActionButton = (FloatingActionButton)findViewById(R.id.road_id);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDirectionPopup();
            }
        });

        NavigationView monumentListNavigationView = (NavigationView)findViewById(R.id.monument_list_nav_view);
        monumentListNavigationView.setNavigationItemSelectedListener(this);

        noMonumentsTextView = (TextView)findViewById(R.id.no_monuments_text_view);
        noMonumentsTextView.setVisibility(View.INVISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        monumentListAdapter = new MonumentListAdapter(this, monumentArrayList, userId);
        monumentListRecylerView = (RecyclerView)findViewById(R.id.monument_list_recycler_view);
        monumentListRecylerView.setLayoutManager(layoutManager);
        monumentListRecylerView.setAdapter(monumentListAdapter);

        loadLocale();
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        map = mapboxMap;
        enableLocation();
        moveCamera(new LatLng(46.0,25.0), INIT_ZOOM);
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected() {
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            currentLocation = location;
            moveCamera(location, DEFAULT_ZOOM);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void openDirectionPopup() {
        GeoDataClient geoDataClient = Places.getGeoDataClient(this, null);

        PlaceAutocompleteAdapter placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, geoDataClient, LAT_LNG_BOUNDS, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MapboxMapActivity.this);
        View setupDirections = getLayoutInflater().inflate(R.layout.pop_window_direction, null);

        builder.setView(setupDirections);
        final AlertDialog alertDialog = builder.create();

        AutoCompleteTextView origin = (AutoCompleteTextView) setupDirections.findViewById(R.id.location_view_id);
        final AutoCompleteTextView destination = (AutoCompleteTextView) setupDirections.findViewById(R.id.wanted_location_id);
        FloatingActionButton confirm = (FloatingActionButton)setupDirections.findViewById(R.id.done_button_id);

        if (currentLocation != null) {
            origin.setText(String.format("%s %s", String.valueOf(currentLocation.getLatitude()), String.valueOf(currentLocation.getLongitude())));
        }else {
            origin.setAdapter(placeAutocompleteAdapter);
        }
        destination.setAdapter(placeAutocompleteAdapter);

        destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                assert inputMethodManager != null;
                inputMethodManager.hideSoftInputFromWindow(destination.getApplicationWindowToken(), 0);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: Create waiting circular progress bar
                //TODO: Place a marker on origin location (with name) - take in account map clearing
                String destinationText = destination.getText().toString();
                if (!destination.getText().toString().isEmpty()) {
                    if (currentLocation == null) {
                        if (!origin.getText().toString().isEmpty()) {
                            com.google.android.gms.maps.model.LatLng googleLatLng = geoLocate(origin.getText().toString());
                            initRoute(new LatLng(googleLatLng.latitude, googleLatLng.longitude), destinationText);
                            alertDialog.dismiss();
                        }else {
                            Toast.makeText(getBaseContext(), "The origin field is empty", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        initRoute(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destinationText);
                        alertDialog.dismiss();
                    }
                }else {
                    Toast.makeText(getBaseContext(), "The destination field is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alertDialog.show();
    }

    private void initRoute(LatLng origin, String destinationText) {
        map.clear();

        com.google.android.gms.maps.model.LatLng destinationGoogleLatLng = geoLocate(destinationText);
        LatLng mapboxDestinationLatLng = new LatLng(destinationGoogleLatLng.latitude, destinationGoogleLatLng.longitude);
        LatLng currentLocationLatLng = new LatLng(origin.getLatitude(), origin.getLongitude());

        primaryPolyline = generateMapboxRoute(MONUMENTS_BY_PREFERENCES_URL, currentLocationLatLng, mapboxDestinationLatLng);

        assert primaryPolyline != null;
        if (!primaryPolyline.isEmpty()) {
            drawPolyline(primaryPolyline, destinationText);
        }
    }

    private void drawPolyline(ArrayList<LatLng>polyline, String destinationName) {
        map.addPolyline(new PolylineOptions()
                .addAll(polyline)
                .color(Color.BLUE)
                .width(2));

        map.addMarker(new MarkerOptions()
                .title(destinationName)
                .position(polyline.get(polyline.size() - 1)));
    }

    @NonNull
    public String generateMapboxQueryUrl (LatLng origin, LatLng destination, boolean withWaypoints){
        String originUrl = origin.getLongitude() + "," + origin.getLatitude();
        String destinationUrl = destination.getLongitude() + "," + destination.getLatitude();
        String travelMode = null;
        String stepsUrl = "steps=true";
        String alternativeRoutesUrl = "alternatives=false";
        String geometriesUrl = "geometries=geojson";
        String accessTokenUrl = "access_token=" + getString(R.string.access_token);

        SharedPreferences sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        if (!sharedPreferences.getString(MapActivity.TRAVEL_MODE, "").equals("")){
            travelMode = sharedPreferences.getString(MapActivity.TRAVEL_MODE, "").toLowerCase();
        }else {
            travelMode = "driving";
        }

        StringBuilder urlParameters = new StringBuilder(travelMode + "/" + originUrl + ";");

        if (withWaypoints) {
            for (LatLng latLng : wayPoints) {
                urlParameters.append(latLng.getLatitude()).append(",").append(latLng.getLongitude()).append(";");
            }
        }

        urlParameters.append(destinationUrl).append("?").append(geometriesUrl).append("&").append(stepsUrl).append("&").append(alternativeRoutesUrl).append("&");

        return MAPBOX_DIRECTIONS_BASE_URL + urlParameters + accessTokenUrl;
    }

    public void reviewMonumentJSONArray(){
        /*ArrayList<com.google.android.gms.maps.model.LatLng> googleBasePolyLine = new ArrayList<>();
        for (int index = 0; index < basePolyLine.size(); ++index){
            com.google.android.gms.maps.model.LatLng latLng = new com.google.android.gms.maps.model.LatLng(basePolyLine.get(index).getLatitude(), basePolyLine.get(index).getLongitude());
            googleBasePolyLine.add(latLng);
        }*/

        if (!wayPoints.isEmpty()) {
            wayPoints.clear();
        }
        for (int index = 0; index < monumentsByPreferencesQueryResponse.length(); ++index){
            /*if (wayPoints.size() >= 11) {
                return;
            }*/
            try {
                JSONObject monument = monumentsByPreferencesQueryResponse.getJSONObject(index);
                com.google.android.gms.maps.model.LatLng monumentLocation = new com.google.android.gms.maps.model.LatLng(monument.getDouble("longitude"), monument.getDouble("latitude"));
                //if (isLocationOnPath(monumentLocation, googleBasePolyLine, false, TOLERANCE)){
                map.addMarker(new MarkerOptions().position(new LatLng(monumentLocation.latitude, monumentLocation.longitude)).title(monument.getString("name")));
                wayPoints.add(new LatLng(monumentLocation.longitude, monumentLocation.latitude));
                //}
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public com.google.android.gms.maps.model.LatLng geoLocate (String locationName){
        Geocoder geocoder = new Geocoder(MapboxMapActivity.this);
        List<Address> addressList = new ArrayList<>();
        try {
            addressList = geocoder.getFromLocationName(locationName, MAX_RESULTS);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addressList.size() > 0){
            Address address = addressList.get(0);
            return new com.google.android.gms.maps.model.LatLng(address.getLatitude(), address.getLongitude());
        }
        else return null;
    }

    public void enableLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            initializeLocationEngine();
            initializeLocationLayer();
        }else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void initializeLocationEngine() {
        locationEngine = new LocationEngineProvider(this).obtainBestLocationEngineAvailable();

        locationEngine.setPriority(LocationEnginePriority.BALANCED_POWER_ACCURACY);
        locationEngine = new LocationEngineProvider(this).obtainBestLocationEngineAvailable();
        locationEngine.addLocationEngineListener(this);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            currentLocation = lastLocation;
            moveCamera(lastLocation, DEFAULT_ZOOM);
        }else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @SuppressLint("WrongConstant")
    @SuppressWarnings("MissingPermission")
    private void initializeLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapView, map, locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.COMPASS);
        getLifecycle().addObserver(locationLayerPlugin);
    }

    public void moveCamera(Location location, float zoom) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoom));
    }

    public void moveCamera (LatLng latLng, float zoom, String locationName){
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(latLng.getLatitude(), latLng.getLongitude())).title(locationName);
        map.addMarker(markerOptions);
    }

    public void moveCamera (LatLng latLng, float zoom){
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            logOut();
        }else if (id == R.id.action_search){
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_map) {
        }
        else if (id == R.id.nav_account) {
            startActivity(new Intent(MapboxMapActivity.this, ProfileActivity.class));
        } else if (id == R.id.nav_preferences) {
            startActivity(new Intent(MapboxMapActivity.this, PreferencesActivity.class));
        } /*else if (id == R.id.nav_qr_scanner) {
            startActivity(new Intent(MapboxMapActivity.this, QrScannerActivity.class));
        } */else if (id == R.id.nav_visited_monuments) {
            startActivity(new Intent(MapboxMapActivity.this, MapboxVisitedMonumentsActivity.class));
        } else if (id == R.id.nav_language) {
            showChangeLanguageDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void generateUrl (){
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        byte[] userEmailToEncode;
        String base64EncodedEmail;
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0, base64EncodedEmail.length() - 1);
        USERS_API_URL = USERS_API_URL + base64EncodedEmail;
    }

    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();
        sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(MapActivity.ROAD_TOLERANCE);
        editor.remove(MapActivity.TRAVEL_MODE);
        editor.apply();
        Intent change = new Intent(MapboxMapActivity.this, MainPageActivity.class);
        startActivity(change);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                com.google.android.gms.maps.model.LatLng latLng = place.getLatLng();
                moveCamera(new LatLng(latLng.latitude, latLng.longitude), DEFAULT_ZOOM, place.getAddress().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void runUserQuery() {
        HttpGet httpGet = new HttpGet(USERS_API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpResponse httpResponse;
        HttpClient httpClient = new DefaultHttpClient();

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    assert jsonObject != null;
                    userId = jsonObject.getInt("id");
                    inputStream.close();
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeaderTitles(UserInformation userInformation) {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        TextView userNameView = (TextView)headerView.findViewById(R.id.user_name_nav);
        userNameView.setText(
                sharedPreferences.getString(
                        SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
        TextView userEmailView = (TextView)headerView.findViewById(R.id.user_email_nav);
        userEmailView.setText(sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, ""));
        CircleImageView profileImageView = (CircleImageView)headerView.findViewById(R.id.profile_image);
        if (userInformation != null) {
            try {
                Glide.with(getBaseContext())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(profileImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    public ArrayList<LatLng> generateMapboxRoute(String API_URL, LatLng origin, LatLng destination) {
        if (!wayPoints.isEmpty()){
            wayPoints.clear();
        }
        HttpGet httpGet = new HttpGet(API_URL);
        ArrayList<LatLng>polyline = new ArrayList<>();

        String travelMode = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE).getString(MapActivity.TRAVEL_MODE, null);

        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        httpGet.addHeader("user_id", String.valueOf(userId));
        httpGet.addHeader("origin_lat", String.valueOf(origin.getLongitude()));
        httpGet.addHeader("origin_lng", String.valueOf(origin.getLatitude()));
        httpGet.addHeader("destination_lat", String.valueOf(destination.getLongitude()));
        httpGet.addHeader("destination_lng", String.valueOf(destination.getLatitude()));
        httpGet.addHeader("road_tolerance", String.valueOf(getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE).getFloat(MapActivity.ROAD_TOLERANCE, 0.0f)));
        httpGet.addHeader("travel_mode", (travelMode != null) ? travelMode : "driving");

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        JSONArray jsonResponse = null;
        JSONArray jsonRoute = null;
        JSONArray jsonWaypoints = null;

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                jsonResponse = convertStreamToJsonArray(inputStream);
                if (jsonResponse != null) {
                    jsonRoute = jsonResponse.getJSONArray(0);
                    jsonWaypoints = jsonResponse.getJSONArray(1);
                }
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        if (jsonRoute != null) {
            for (int index = 0; index < jsonRoute.length(); ++index) {
                JSONObject coordinates = null;
                try {
                    coordinates = jsonRoute.getJSONObject(index);
                    polyline.add(new LatLng(
                            coordinates.getDouble("lat"),
                            coordinates.getDouble("lng")
                    ));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (jsonWaypoints != null) {
            if (!monumentArrayList.isEmpty()) {
                monumentArrayList.clear();
                monumentListAdapter.notifyDataSetChanged();
            }
            for (int index = 0; index < jsonWaypoints.length(); ++index) {
                JSONObject monument = null;
                try {
                    monument = jsonWaypoints.getJSONObject(index);
                    map.addMarker(new MarkerOptions()
                            .position(new LatLng(monument.getDouble("lng"), monument.getDouble("lat")))
                            .title(monument.getString("name")));
                    wayPoints.add(new LatLng(monument.getDouble("lng"), monument.getDouble("lat")));
                    monumentArrayList.add(new Monument(
                            monument.getInt("monument_id"),
                            monument.getString("name"),
                            monument.getString("image_url"),
                            (float) monument.getDouble("average_rating"),
                            new com.example.razvan.google.model.Location(
                                    (float) monument.getDouble("lng"), (float) monument.getDouble("lat")
                            ),
                            monument.getBoolean("is_visited")
                    ));
                    //monumentListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        monumentListAdapter.notifyDataSetChanged();
        return polyline;
    }

    @Nullable
    public UserInformation getUserInformationFromDatabase(String API_URL) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        return userInformation;
    }

    @NonNull
    public JSONObject runMapboxDirectionsQuery(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        JSONObject responseJson = new JSONObject();

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                responseJson = convertStreamToJsonOblect(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseJson;
    }

    public void runMonumentByPreferencesQuery(String API_URL, String baserMapboxApiUrl) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        httpGet.addHeader("user_id", String.valueOf(userId));
        httpGet.addHeader("map_request_url", baserMapboxApiUrl);
        httpGet.addHeader("road_tolerance", String.valueOf(getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE).getFloat(MapActivity.ROAD_TOLERANCE, 0.0f)));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                monumentsByPreferencesQueryResponse = convertStreamToJsonArray(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            httpGet.releaseConnection();
        }
    }

    @NonNull
    public JSONArray convertArrayToJsonArray(ArrayList<LatLng>array) {
        JSONArray jsonArray = new JSONArray();
        for (LatLng location : array) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("longitude", location.getLongitude());
                jsonObject.put("latitude", location.getLatitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    @NonNull
    public String convertJSONArrayToString(JSONArray jsonArray) {
        return jsonArray.toString();
    }

    @NonNull
    public static Bitmap convertStringToBitmap (String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showChangeLanguageDialog() {
        final String[] listItems = {"English", "Roumanian", "Hindi", "Russian", "Mandarin", "French", "Hungarian", "Greek", "German"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(MapboxMapActivity.this);
        builder.setTitle("Select language");
        builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        setLocale("en");
                        recreate();
                        break;
                    case 1:
                        setLocale("ro");
                        recreate();
                        break;
                    case 2:
                        setLocale("hi");
                        recreate();
                        break;
                    case 3:
                        setLocale("ru");
                        recreate();
                        break;
                    case 4:
                        setLocale("zh");
                        recreate();
                        break;
                    case 5:
                        setLocale("fr");
                        recreate();
                        break;
                    case 6:
                        setLocale("hu");
                        recreate();
                        break;
                    case 7:
                        setLocale("el");
                        recreate();
                        break;
                    case 8:
                        setLocale("de");
                        recreate();
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates();
        }
        if (locationLayerPlugin != null) {
            locationLayerPlugin.onStop();
        }
        mapView.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @SuppressWarnings("MissingPermission")
    @Override
    protected void onStart() {
        super.onStart();
        if (locationEngine != null) {
            locationEngine.requestLocationUpdates();
        }
        if (locationLayerPlugin != null) {
            locationLayerPlugin.onStart();
        }

        mapView.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationEngine != null) {
            locationEngine.deactivate();
        }
        mapView.onDestroy();
    }

}

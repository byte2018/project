package com.example.razvan.google.profile;

import android.view.View;

public interface ItemClickListener {
    void onClick (View view, int position, boolean isLongClicked);
}

package com.example.razvan.google;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.example.razvan.google.auth.SignInActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrScannerActivity extends AppCompatActivity {

    public static final String API_BASE_URL = "http://192.168.137.1:8000/ticket/add";
    public String USER_ID_QUERY_BASE_URL = "http://192.168.137.1:8000/user/get-email/";
    public String QR_QUERY_BASE_URL = "http://192.168.137.1:8000/qrcode/view/get/";
    public String MONUMENT_QUERY_BASE_URL = "http://192.168.137.1:8000/monument/view/";
    public static final int DEFAULT_JSON_ARRAY_INDEX = 0;

    public List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
    public String currentDate;
    public int monumentId;
    public int userId;
    public int qrCodeId;

    public HttpClient httpClient = new DefaultHttpClient();
    public HttpPost httpPost = new HttpPost(API_BASE_URL);
    public HttpResponse httpResponse;

    private ZXingScannerView zXingScannerView;
    private Vibrator vibrator;
    public String resultCode;
    public Bitmap qrCodeImageFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scanner);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        zXingScannerView = new ZXingScannerView(this);
        zXingScannerView.setResultHandler(new ZXingResultHandler());

        setContentView(zXingScannerView);
        zXingScannerView.startCamera();
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject runHtpGetQuery(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        JSONObject jsonObject = new JSONObject();
        try {
            StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(threadPolicy);
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                jsonObject = convertStreamToJsonOblect(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray runHtpGetQueryInArrayForm(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        JSONArray jsonArray = new JSONArray();
        try {
            StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(threadPolicy);
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                jsonArray = convertStreamToJsonArray(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public UrlEncodedFormEntity createHttpPostEntity() throws UnsupportedEncodingException {
        nameValuePairs.add(new BasicNameValuePair("date", currentDate));
        nameValuePairs.add(new BasicNameValuePair("monument_id", Integer.toString(monumentId)));
        nameValuePairs.add(new BasicNameValuePair("user_id", Integer.toString(userId)));
        nameValuePairs.add(new BasicNameValuePair("qr_code_id", Integer.toString(qrCodeId)));

        return new UrlEncodedFormEntity(nameValuePairs);
    }

    public void runHttpPost() {
        httpPost.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            httpPost.setEntity(createHttpPostEntity());
            StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(threadPolicy);
            httpResponse = httpClient.execute(httpPost);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        httpPost.releaseConnection();
    }

    private String createUserQueryUrl(String API_URL) {
        String API_FINAL_URL = API_URL;
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        byte[] userEmailToEncode;
        String base64EncodedEmail;
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0, base64EncodedEmail.length() - 1);
        API_FINAL_URL = API_FINAL_URL + base64EncodedEmail;
        return API_FINAL_URL;
    }

    @Override
    protected void onPause() {
        super.onPause();
        zXingScannerView.stopCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
        zXingScannerView.stopCamera();
    }

    class ZXingResultHandler implements ZXingScannerView.ResultHandler {

        @Nullable
        private Bitmap generateQrCode() {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(resultCode, BarcodeFormat.QR_CODE, 200, 200);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                return barcodeEncoder.createBitmap(bitMatrix);
            } catch (WriterException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void handleResult(Result result) {
            resultCode = result.getText();

            QR_QUERY_BASE_URL = QR_QUERY_BASE_URL + resultCode;
            JSONArray jsonArray = runHtpGetQueryInArrayForm(QR_QUERY_BASE_URL);
            JSONObject qrCodeQueryJSONResult = new JSONObject();
            try {
                qrCodeQueryJSONResult = jsonArray.getJSONObject(DEFAULT_JSON_ARRAY_INDEX);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                assert qrCodeQueryJSONResult != null;
                monumentId = qrCodeQueryJSONResult.getInt("monument_id");
                qrCodeId = qrCodeQueryJSONResult.getInt("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject userDatabaseQueryResult = runHtpGetQuery(createUserQueryUrl(USER_ID_QUERY_BASE_URL));
            try {
                userId = userDatabaseQueryResult.getInt("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            vibrator.vibrate(500);

            Calendar calendar = Calendar.getInstance();
            currentDate = DateFormat.getDateInstance().format(calendar.getTime());
            runHttpPost();

            Intent intent = new Intent(QrScannerActivity.this, TicketActivity.class);
            intent.putExtra("resultCode", resultCode);
            intent.putExtra("currentDate", currentDate);

            MONUMENT_QUERY_BASE_URL = MONUMENT_QUERY_BASE_URL + Integer.toString(monumentId);
            JSONObject monumentQueryJSONFormat = runHtpGetQuery(MONUMENT_QUERY_BASE_URL);
            System.out.println(monumentQueryJSONFormat);
            try {
                intent.putExtra("monumentName", monumentQueryJSONFormat.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            startActivity(intent);
        }
    }

    /*public method convertImgToString(img image) {
        Image img = getImageFromUploadForm();
        String image = convertToString(img);
        laravel.saveImage(image);
    }

    public Img retriveImage()
    {
        String imgaSsRING = laravel.getImage();
        convertStringToImg(imgaSsRING);
    }*/
}

package com.example.razvan.google.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.razvan.google.R;
import com.example.razvan.google.TicketActivity;
import com.example.razvan.google.model.Ticket;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.IOException;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpDelete;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketViewHolder>{

    private static final int HTTP_RESULT_CODE_OK = 200;
    private String DELETE_URL = "http://192.168.137.1:8000/ticket/delete/";

    private HttpClient httpClient = new DefaultHttpClient();
    private HttpResponse httpResponse;

    private Context context;
    private List<Ticket>ticketList;

    public TicketAdapter(Context context, List<Ticket> ticketList) {
        this.context = context;
        this.ticketList = ticketList;
    }

    @Override
    public TicketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.ticket_library_card_view, null);
        return new TicketViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TicketViewHolder holder, int position) {
        final Ticket ticket = ticketList.get(position);

        holder.imageView.setImageBitmap(generateQrCode(ticket.qrCode));
        holder.ticketTitle.setText(ticket.getMonumentName());
        holder.ticketDate.setText(ticket.getDate());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClicked) {
                if (isLongClicked){
                    showAlertDialogForDelete(ticket.id);
                }else {
                    setTicketViewContent(ticket);
                }
            }
        });
    }

    private void showAlertDialogForDelete(final int ticketId) {
        final String[] itemList = {"Delete"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        builder.setSingleChoiceItems(itemList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                switch (index){
                    case 0:
                        deleteTicket(ticketId);
                        break;
                }
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteTicket(int id) {
        DELETE_URL += id;
        HttpDelete httpDelete = new HttpDelete(DELETE_URL);
        try {
            httpResponse = httpClient.execute(httpDelete);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESULT_CODE_OK) {
                //TODO: recreate fragment
                Toast.makeText(context, "Successful deleted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setTicketViewContent(Ticket ticket) {
        Intent intent = new Intent(context, TicketActivity.class);
        intent.putExtra("resultCode", ticket.qrCode);
        intent.putExtra("monumentName", ticket.monumentName);
        intent.putExtra("currentDate", ticket.date);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    @Nullable
    private Bitmap generateQrCode(String qrCode){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

        try{
            BitMatrix bitMatrix = multiFormatWriter.encode(qrCode, BarcodeFormat.QR_CODE, 120, 90);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (WriterException e){
            e.printStackTrace();
        }
        return null;
    }

    class TicketViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        
        ImageView imageView;
        TextView ticketTitle;
        TextView ticketDate;

        private ItemClickListener itemClickListener;

        public TicketViewHolder(View itemView) {
            super(itemView);
            
            imageView = itemView.findViewById(R.id.ticket_image);
            ticketTitle = itemView.findViewById(R.id.ticket_title);
            ticketDate = itemView.findViewById(R.id.ticket_date);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setItemClickListener (ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }
    
}

package com.example.razvan.google.model;

public class Profile {

    public double distance;
    public double numberOfCitiesVisited;
    public double numberOfMonumentsVisited;
    public String mostVisitedCity;
    public String mostVisitedTypeOfMonument;


    public void setDistance(double distance) { this.distance = distance; }

    public double getDistance() { return distance; }

    public void setNumberOfCitiesVisited(double numberOfCitiesVisited) {
        this.numberOfCitiesVisited = numberOfCitiesVisited;
    }

    public double getNumberOfCitiesVisited() {
        return numberOfCitiesVisited;
    }

    public void setNumberOfMonumentsVisited(double numberOfMonumentsVisited) {
        this.numberOfMonumentsVisited = numberOfMonumentsVisited;
    }

    public double getNumberOfMonumentsVisited() {
        return numberOfMonumentsVisited;
    }

    public void setMostVisitedCity(String mostVisitedCity) {
        this.mostVisitedCity = mostVisitedCity;
    }

    public String getMostVisitedCity() {
        return mostVisitedCity;
    }

    public void setMostVisitedTypeOfMonument(String mostVisitedTypeOfMonument) {
        this.mostVisitedTypeOfMonument = mostVisitedTypeOfMonument;
    }

    public String getMostVisitedTypeOfMonument() {
        return mostVisitedTypeOfMonument;
    }

}


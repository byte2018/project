package com.example.razvan.google.monument;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.Comment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpDelete;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.hdodenhof.circleimageview.CircleImageView;

public class MonumentCommentAdapter extends RecyclerView.Adapter<MonumentCommentAdapter.CommentViewHolder> {

    private static final String BASE_API_URL = "http://192.168.137.1:8000/monument-image/view-image/";
    private static final int HTTP_RESULT_CODE_OK = 200;
    private String DELETE_URL = "http://192.168.137.1:8000/feedback/delete/";

    private Context context;
    private List<Comment>commentList;
    private int userId;

    public MonumentCommentAdapter(Context context, List<Comment> commentList, int userId) {
        this.context = context;
        this.commentList = commentList;
        this.userId = userId;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.monument_comment_card_view, null);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
        Comment comment = commentList.get(position);

        holder.userNameView.setText(comment.getUserName());
        holder.commentView.setText(comment.getComment());
        if (!comment.getUserProfileImagePath().equals("null")) {
            try {
                Glide.with(context)
                        .load(BASE_API_URL + Base64.encodeToString(URLEncoder.encode(comment.getUserProfileImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .override(1000, 1100)
                        .centerCrop()
                        .into(holder.circleImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            holder.circleImageView.setImageResource(R.drawable.logo_view);
        }

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClicked) {
                if (isLongClicked && userId == comment.getUserId()) {
                    showAlertDialogForDelete(comment.getId(), position);
                } else {
                    Toast.makeText(context, "You don't have permission to delete this comment", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showAlertDialogForDelete(final int commentId, final int commentIndex) {
        final String[] itemList = {"Delete"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Options");
        builder.setSingleChoiceItems(itemList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int index) {
                if (index == 0) {
                    deleteTicket(commentId, commentIndex);
                }
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteTicket(int id, int commentIndex) {
        DELETE_URL += id;
        HttpClient httpClient = new DefaultHttpClient();
        HttpDelete httpDelete = new HttpDelete(DELETE_URL);
        httpDelete.setHeader("api_token", context.getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        try {
            HttpResponse httpResponse = httpClient.execute(httpDelete);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESULT_CODE_OK) {
                Toast.makeText(context, "Deleted successfully", Toast.LENGTH_SHORT).show();
                commentList.remove(commentIndex);
                this.notifyDataSetChanged();
            } else {
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        TextView userNameView;
        TextView commentView;
        CircleImageView circleImageView;

        private ItemClickListener itemClickListener;

        public CommentViewHolder(View itemView) {
            super(itemView);

            userNameView = itemView.findViewById(R.id.user_name_id);
            commentView = itemView.findViewById(R.id.comment_message_view_id);
            circleImageView = itemView.findViewById(R.id.comment_profile_image);

            //itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }

}

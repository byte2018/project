package com.example.razvan.google.map.google_api;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.model.Direction;
import com.example.razvan.google.MainPageActivity;
import com.example.razvan.google.PreferencesActivity;
import com.example.razvan.google.QrScannerActivity;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;
import com.example.razvan.google.profile.ProfileActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.google.maps.android.PolyUtil.isLocationOnPath;

public class MapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, DirectionCallback {

    public static final String API_QUERY_URL = "http://192.168.137.1:8000/monument/get-all";
    public static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/directions/";
    public static final String GOOGLE_API_KEY = "AIzaSyC9Nv0_QxehcW2gj6ZHI6l_3MVxqmyuAag";
    public String USERS_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String MONUMENTS_BY_PREFERENCES_URL = "http://192.168.137.1:8000/monument/get-by-preferences/";
    public String USER_INFORMATION_URL = "http://192.168.137.1:8000/user-information/view-information/";
    private static final float DEFAULT_ZOOM = 15f;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-40, -168), new LatLng(71, 136));
    private static final int MAX_RESULTS = 1;
    private static final int CONVERSION_CONSTANT = 1000;
    public double TOLERANCE = 10000;

    public static final String MAP_PREFERENCES = "Map Preferences";
    public static final String ROAD_TOLERANCE = "Road Tolerance";
    public static final String TRAVEL_MODE = "Travel Mode";

    public int userId;

    HttpClient httpClient = new DefaultHttpClient();
    HttpResponse httpResponse;

    public JSONArray monumentQueryResponse = new JSONArray();
    public JSONObject googleDirectionQueryResponse;
    public ArrayList<LatLng>basePolyLine = new ArrayList<LatLng>();
    public List<LatLng>wayPoints = new ArrayList<LatLng>();

    private GoogleMap mMap;

    private FusedLocationProviderClient fusedLocationProviderClient;
    public Location currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        generateUrl();
        runUserQuery();
        MONUMENTS_BY_PREFERENCES_URL += userId;
        USER_INFORMATION_URL += userId;

        setNavigationHeaderTitles();

        runQuery();
        loadLocale();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.road_id);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDirectionPopup();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        SharedPreferences sharedPreferences = getSharedPreferences(MAP_PREFERENCES, Context.MODE_PRIVATE);
        float sharedTolerance = sharedPreferences.getFloat(ROAD_TOLERANCE, 0.0f);
        if (sharedTolerance != 0){
            TOLERANCE = sharedTolerance * CONVERSION_CONSTANT;
        }
    }

    private void openDirectionPopup() {
        GeoDataClient geoDataClient = Places.getGeoDataClient(this, null);

        PlaceAutocompleteAdapter placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, geoDataClient, LAT_LNG_BOUNDS, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
        View setupDirections = getLayoutInflater().inflate(R.layout.pop_window_direction, null);

        builder.setView(setupDirections);
        final AlertDialog alertDialog = builder.create();

        EditText origin = (EditText)setupDirections.findViewById(R.id.location_view_id);
        final AutoCompleteTextView destination = (AutoCompleteTextView) setupDirections.findViewById(R.id.wanted_location_id);
        FloatingActionButton confirm = (FloatingActionButton)setupDirections.findViewById(R.id.done_button_id);

        origin.setText(String.format("%s %s", String.valueOf(currentLocation.getLatitude()), String.valueOf(currentLocation.getLongitude())));
        destination.setAdapter(placeAutocompleteAdapter);

        destination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                assert inputMethodManager != null;
                inputMethodManager.hideSoftInputFromWindow(destination.getApplicationWindowToken(), 0);
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.clear();
                //TODO: Create waiting circular progress bar
                String destinationText = destination.getText().toString();
                initRoute(destinationText);
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public LatLng geoLocate (String locationName){
        Geocoder geocoder = new Geocoder(MapActivity.this);
        List<Address> addressList = new ArrayList<>();
        try {
            addressList = geocoder.getFromLocationName(locationName, MAX_RESULTS);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addressList.size() > 0){
            Address address = addressList.get(0);
            return new LatLng(address.getLatitude(), address.getLongitude());
        }
        else return null;
    }

    private void initRoute(String destination) {
        LatLng destinationLatLng = geoLocate(destination);

        MarkerOptions markerOptions = new MarkerOptions().position(destinationLatLng).title(destination);
        mMap.addMarker(markerOptions);

        String apiUrl = getDirectionUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destinationLatLng, false);
        runGoogleQuery(apiUrl);

        DirectionsJSONParser directionsJSONParser = new DirectionsJSONParser();
        List<List<HashMap<String, String>>>routes = directionsJSONParser.parse(googleDirectionQueryResponse);
        List<HashMap<String, String>>path = routes.get(0);
        for (int index = 0; index < path.size(); ++index){
            HashMap<String, String> point = path.get(index);
            LatLng position = new LatLng(Double.parseDouble(point.get("lat")), Double.parseDouble(point.get("lng")));
            basePolyLine.add(position);
        }
        reviewMonumentJSONArray(basePolyLine);

        routes.clear();
        path.clear();

        String finalGoogleApiUrl = getDirectionUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destinationLatLng, true);
        finalGoogleApiUrl = finalGoogleApiUrl.replace("|", "%7C");
        runGoogleQuery(finalGoogleApiUrl);
        routes = directionsJSONParser.parse(googleDirectionQueryResponse);
        path = routes.get(0);
        ArrayList<LatLng>finalPolyline = new ArrayList<LatLng>();
        for (int index = 0; index < path.size(); ++index){
            HashMap<String, String> point = path.get(index);
            LatLng position = new LatLng(Double.parseDouble(point.get("lat")), Double.parseDouble(point.get("lng")));
            finalPolyline.add(position);
        }

        PolylineOptions polylineOptions = new PolylineOptions().addAll(finalPolyline).width(5).color(Color.BLUE);
        mMap.addPolyline(polylineOptions);
    }

    @NonNull
    private String getDirectionUrl (LatLng origin, LatLng destination, boolean withWaypoints){
        String originUrl = "origin=" + origin.latitude + "," + origin.longitude;
        String destinationUrl = "destination=" + destination.latitude + "," + destination.longitude;
        String travelMode = "&mode=";
        String urlParameters = originUrl + "&" + destinationUrl;

        if (withWaypoints) {
            String waypointsUrl = "waypoints=";
            for (int index = 0; index < wayPoints.size(); ++index) {
                LatLng point = wayPoints.get(index);
                waypointsUrl += point.latitude + "," + point.longitude + "|";
            }
            waypointsUrl = waypointsUrl.substring(0, waypointsUrl.length()-1);
            urlParameters += "&" + waypointsUrl;
        }

        SharedPreferences sharedPreferences = getSharedPreferences(MAP_PREFERENCES, Context.MODE_PRIVATE);
        String s1 = sharedPreferences.getString(TRAVEL_MODE, "");
        if (!sharedPreferences.getString(TRAVEL_MODE, "").equals("")){
            travelMode += sharedPreferences.getString(TRAVEL_MODE, "").toLowerCase();
            urlParameters += travelMode;
        }

        String outputFormat = "json";

        return GOOGLE_BASE_URL + outputFormat + "?" + urlParameters + "&key=" + GOOGLE_API_KEY;
    }

    private void runGoogleQuery(String API_URL) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(API_URL);
        try{
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                googleDirectionQueryResponse = convertStreamToJsonObject(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    private JSONObject convertStreamToJsonObject(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            logOut();
            return true;
        } else if (id == R.id.action_search){
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                moveCamera(place.getLatLng(), DEFAULT_ZOOM, place.getAddress().toString());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();
        sharedPreferences = getSharedPreferences(MAP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(TRAVEL_MODE);
        editor.remove(ROAD_TOLERANCE);
        editor.apply();
        Intent change = new Intent(MapActivity.this, MainPageActivity.class);
        startActivity(change);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            //TODO: Delete Mapbox reference from here
            Intent change = new Intent(MapActivity.this, MapboxMapActivity.class);
            startActivity(change);
        } else if (id == R.id.nav_account) {
            Intent change = new Intent(MapActivity.this, ProfileActivity.class);
            startActivity(change);
        } else if (id == R.id.nav_preferences) {
            Intent change = new Intent(MapActivity.this, PreferencesActivity.class);
            startActivity(change);
        } /*else if (id == R.id.nav_qr_scanner) {
            Intent change = new Intent(MapActivity.this, QrScannerActivity.class);
            startActivity(change);
        }*/ else if (id == R.id.nav_language) {
            showChangeLanguageDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            android.support.v4.app.ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 1);
            return;
        }

        getDeviceLocation();

        mMap.setMyLocationEnabled(true);

        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setCompassEnabled(true);
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeaderTitles() {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        TextView userNameView = (TextView)headerView.findViewById(R.id.user_name_nav);
        userNameView.setText(
                sharedPreferences.getString(
                        SignInActivity.ACCOUNT_FIRST_NAME, "") + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
        TextView userEmailView = (TextView)headerView.findViewById(R.id.user_email_nav);
        userEmailView.setText(sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, ""));
        CircleImageView profileImageView = (CircleImageView)headerView.findViewById(R.id.profile_image);
        if (getUSerProfileImage() != null) {
            profileImageView.setImageBitmap(null);
            profileImageView.setImageBitmap(getUSerProfileImage());
        }
    }

    @NonNull
    public static Bitmap convertStringToBitmap (String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    public Bitmap getUSerProfileImage() {
        HttpGet httpGet = new HttpGet(USER_INFORMATION_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        String imageInStringFormat = null;

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                if (jsonArray != null) {
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    assert jsonObject != null;
                    imageInStringFormat = jsonObject.getString("image");
                }
                inputStream.close();
            }else {
                return null;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (imageInStringFormat != null) {
            return convertStringToBitmap(imageInStringFormat);
        }
        return null;
    }

    public void getDeviceLocation (){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try{
            Task<Location> tasks = fusedLocationProviderClient.getLastLocation();
            tasks.addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()){
                        currentLocation = task.getResult();
                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);
                    }
                }
            });
        } catch (SecurityException e){
            e.printStackTrace();
        }
    }

    public void moveCamera (LatLng latLng, float zoom){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void moveCamera (LatLng latLng, float zoom, String locationName){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(locationName);
        mMap.addMarker(markerOptions);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void runQuery() {
        HttpGet httpGet = new HttpGet(MONUMENTS_BY_PREFERENCES_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                monumentQueryResponse = convertStreamToJsonArray(inputStream);
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reviewMonumentJSONArray(ArrayList<LatLng> basePolyLine){
        for (int index = 0; index < monumentQueryResponse.length(); ++index){
            try {
                JSONObject monument = monumentQueryResponse.getJSONObject(index);
                LatLng monumentLocation = new LatLng(monument.getDouble("longitude"), monument.getDouble("latitude"));
                if (isLocationOnPath(monumentLocation, basePolyLine, false, TOLERANCE)){
                    mMap.addMarker(new MarkerOptions().position(monumentLocation).title(monument.getString("name")));
                    wayPoints.add(monumentLocation);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {

    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }

    public void showChangeLanguageDialog() {
        final String[] listItems = {"English", "Roumanian", "Hindi", "Russian", "Mandarin", "French", "Hungarian", "Greek", "German"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
        builder.setTitle("Select language");
        builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        setLocale("en");
                        recreate();
                        break;
                    case 1:
                        setLocale("ro");
                        recreate();
                        break;
                    case 2:
                        setLocale("hi");
                        recreate();
                        break;
                    case 3:
                        setLocale("ru");
                        recreate();
                        break;
                    case 4:
                        setLocale("zh");
                        recreate();
                        break;
                    case 5:
                        setLocale("fr");
                        recreate();
                        break;
                    case 6:
                        setLocale("hu");
                        recreate();
                        break;
                    case 7:
                        setLocale("el");
                        recreate();
                        break;
                    case 8:
                        setLocale("de");
                        recreate();
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }


    public void generateUrl (){
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        byte[] userEmailToEncode;
        String base64EncodedEmail;
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0, base64EncodedEmail.length() - 1);
        USERS_API_URL = USERS_API_URL + base64EncodedEmail;
    }

    private void runUserQuery() {
        HttpGet httpGet = new HttpGet(USERS_API_URL);
        httpGet.addHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpResponse httpResponse;
        HttpClient httpClient = new DefaultHttpClient();

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            //if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null){
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    assert jsonObject != null;
                    userId = jsonObject.getInt("id");
                    inputStream.close();
                }
           // }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

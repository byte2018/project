package com.example.razvan.google.monument_image;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.razvan.google.R;
import com.example.razvan.google.model.MonumentImage;
import com.example.razvan.google.monument_image.ui.main.SectionsPagerAdapter;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    private ArrayList<MonumentImage>monumentImageArrayList = new ArrayList<>();
    private int position;

    private int monumentId;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        monumentImageArrayList = getIntent().getParcelableArrayListExtra("imageList");
        position = getIntent().getIntExtra("position", -1);
        userId = getIntent().getIntExtra("user_id", 0);
        monumentId = getIntent().getIntExtra("monument_id", 0);

        Intent intent = new Intent(DetailActivity.this, MonumentImageActivity.class);
        intent.putExtra("user_id", userId);
        intent.putExtra("monument_id", monumentId);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), monumentImageArrayList, position);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTitle(String.valueOf(monumentImageArrayList.get(position).getId()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(DetailActivity.this, MonumentImageActivity.class);
            intent.putExtra("user_id", userId);
            intent.putExtra("monument_id", monumentId);
            startActivity(intent);
        }
        return true;
    }
}
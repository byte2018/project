package com.example.razvan.google.model;

public class User
{
    private String firstName;
    private String lastName;
    public String email;
    private String password;
    private String apiToken;

    public User(String firstName, String lastName, String email, String apiToken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.apiToken = apiToken;
    }

    public User(String firstName, String lastName, String email, String password, String apiToken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.apiToken = apiToken;
    }

    public User() {
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }
}

package com.example.razvan.google.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.R;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cz.msebera.android.httpclient.ConnectionClosedException;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class SignInActivity extends AppCompatActivity {

    public static final String API_BASE_URL =  "http://192.168.137.1:8000/user/add";
    private static final int HTTP_RESPONSE_OK = 200;
    private static final int HTTP_RESPONSE_QUERY_EXCEPTION = 409;
    private String USER_API_URL = "http://192.168.137.1:8000/user/check/";

    public EditText lastNameField;
    public EditText firstNameField;
    public EditText emailField;
    public EditText passwordField;
    public Button signInButton;

    public String firstName;
    public String lastName;
    public String email;
    public String pass;

    private int userId;

    public List<NameValuePair>nameValuePairs = new ArrayList<NameValuePair>(4);

    HttpClient httpClient = new DefaultHttpClient();
    HttpPost httpPost = new HttpPost(API_BASE_URL);
    HttpResponse httpResponse;

    public static final String ACCOUNT_PREFERENCES = "Account Preferences";
    public static final String ACCOUNT_EMAIL = "EmailKey";
    public static final String ACCOUNT_PASSWORD = "PasswordKey";
    public static final String ACCOUNT_API_TOKEN = "ApiTokenKey";
    public static final String ACCOUNT_FIRST_NAME = "FirstNameKey";
    public static final String ACCOUNT_LAST_NAME = "LastNameKey";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setField();
        loadLocale();
        signIn();
    }

    public void setField(){
        firstNameField=(EditText)findViewById(R.id.first_name_id);
        lastNameField=(EditText)findViewById(R.id.last_name_id);
        emailField=(EditText)findViewById(R.id.e_mail_id);
        passwordField=(EditText)findViewById(R.id.password_id);
        signInButton=(Button) findViewById(R.id.sign_in_id);

    }

    public void getSugnUpData (){
        firstName = firstNameField.getText().toString();
        lastName = lastNameField.getText().toString();
        email = emailField.getText().toString();
        pass = passwordField.getText().toString();
    }

    @android.support.annotation.Nullable
    private String md5Encrypt(@NonNull String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes());
            byte[] encoded = messageDigest.digest();
            return URLEncoder.encode(new String(encoded), "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getSignUpForm() throws UnsupportedEncodingException {
        nameValuePairs.add(new BasicNameValuePair("user_first_name", firstName));
        nameValuePairs.add(new BasicNameValuePair("user_last_name", lastName));
        nameValuePairs.add(new BasicNameValuePair("user_email", email));
        nameValuePairs.add(new BasicNameValuePair("user_password", md5Encrypt(pass)));

        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    }

    public void signIn(){
        sharedPreferences = getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    getSugnUpData();
                    try {
                        getSignUpForm();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(threadPolicy);
                    httpResponse = httpClient.execute(httpPost);
                    if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_QUERY_EXCEPTION) {
                        Toast.makeText(getBaseContext(), "Failed. Email already exists.", Toast.LENGTH_SHORT).show();
                    }else if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK){
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(ACCOUNT_FIRST_NAME, firstNameField.getText().toString());
                        editor.putString(ACCOUNT_LAST_NAME, lastNameField.getText().toString());
                        editor.putString(ACCOUNT_EMAIL, emailField.getText().toString());
                        editor.putString(ACCOUNT_API_TOKEN, getApiToken(generateUrl()));
                        editor.apply();
                        Intent change = new Intent(SignInActivity.this, MapboxMapActivity.class);
                        startActivity(change);
                    }
                }catch (ConnectionClosedException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }finally {
                    httpPost.releaseConnection();
                }
            }
        });
    }

    private String getApiToken(String API_URL) {
        HttpGet httpGet = new HttpGet(API_URL);
        HttpResponse httpResponse;
        HttpClient httpClient = new DefaultHttpClient();
        String apiToken = null;

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    apiToken = Objects.requireNonNull(convertStreamToJsonOblect(inputStream)).getString("api_token");
                    inputStream.close();
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            httpGet.releaseConnection();
        }
        return apiToken;
    }

    @NonNull
    private String generateUrl() {
        email = emailField.getText().toString();
        byte[] userEmailToEncode = email.getBytes();
        String base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        String password = passwordField.getText().toString();
        return USER_API_URL + base64EncodedEmail + "/" + md5Encrypt(password);
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

}

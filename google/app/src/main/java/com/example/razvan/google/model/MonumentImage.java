package com.example.razvan.google.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MonumentImage implements Parcelable {

    private int id;
    private String serverImagePath;
    private int monumentId;
    private int userId;

    public MonumentImage(int id, String serverImagePath, int monumentId, int userId) {
        this.id = id;
        this.serverImagePath = serverImagePath;
        this.monumentId = monumentId;
        this.userId = userId;
    }

    public MonumentImage(String serverImagePath, int monumentId, int userId) {
        this.serverImagePath = serverImagePath;
        this.monumentId = monumentId;
        this.userId = userId;
    }

    protected MonumentImage(Parcel in) {
        id = in.readInt();
        serverImagePath = in.readString();
        monumentId = in.readInt();
        userId = in.readInt();
    }

    public static final Creator<MonumentImage> CREATOR = new Creator<MonumentImage>() {
        @Override
        public MonumentImage createFromParcel(Parcel in) {
            return new MonumentImage(in);
        }

        @Override
        public MonumentImage[] newArray(int size) {
            return new MonumentImage[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getServerImagePath() {
        return serverImagePath;
    }

    public void setServerImagePath(String serverImagePath) {
        this.serverImagePath = serverImagePath;
    }

    public int getMonumentId() {
        return monumentId;
    }

    public void setMonumentId(int monumentId) {
        this.monumentId = monumentId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(serverImagePath);
        dest.writeInt(monumentId);
        dest.writeInt(userId);
    }
}

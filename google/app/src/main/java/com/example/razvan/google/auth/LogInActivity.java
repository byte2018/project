package com.example.razvan.google.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.razvan.google.R;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;
import com.loopj.android.http.HttpGet;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class LogInActivity extends AppCompatActivity {

    public String API_BASE_URL = "http://192.168.137.1:8000/user/check/";
    public String API_FINAL_URL;

    public EditText emailField;
    public EditText passwordField;
    public Button logInButton;
    public CheckBox rememberMe;

    public String email;
    public String password;

    byte[] userEmailToEncode;
    String base64EncodedEmail;

    HttpClient httpClient = new DefaultHttpClient();
    HttpResponse httpResponse;

    SharedPreferences sharedPreferences;

    public void setField (){
        emailField = (EditText)findViewById(R.id.e_mail_id);
        passwordField = (EditText)findViewById(R.id.password_id);
        logInButton = (Button)findViewById(R.id.log_in_id);
        rememberMe = (CheckBox)findViewById(R.id.remembetr_me);
        rememberMe.setChecked(true);
    }

    @Nullable
    private String md5Encrypt(@NonNull String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes());
            byte[] encoded = messageDigest.digest();
            return URLEncoder.encode(new String(encoded), "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getLogInForm () {
        email = emailField.getText().toString();
        userEmailToEncode = email.getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        password = passwordField.getText().toString();
        API_FINAL_URL = API_BASE_URL + base64EncodedEmail + "/" + md5Encrypt(password);
    }

    private static boolean convertStreamToBoolean (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw  e;
        }finally {
            try{
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw  e;
            }
        }
        return stringBuilder.toString().equals("1");
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void logIn(){
        sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLogInForm();
                HttpGet httpGet = new HttpGet(API_FINAL_URL);
                try{
                    StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(threadPolicy);
                    httpResponse = httpClient.execute(httpGet);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null){
                        InputStream inputStream = httpEntity.getContent();
                        //Boolean result = convertStreamToBoolean(inputStream);
                        JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                        System.out.println(jsonObject);
                        assert jsonObject != null;
                        if(jsonObject.has("id")) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            if (rememberMe.isChecked()) {
                                editor.putString(SignInActivity.ACCOUNT_FIRST_NAME, jsonObject.getString("user_first_name"));
                                editor.putString(SignInActivity.ACCOUNT_LAST_NAME, jsonObject.getString("user_last_name"));
                                editor.putString(SignInActivity.ACCOUNT_EMAIL, email);
                            }
                            editor.putString(SignInActivity.ACCOUNT_API_TOKEN, jsonObject.getString("api_token"));
                            editor.apply();
                            Intent change = new Intent(LogInActivity.this, MapboxMapActivity.class);
                            startActivity(change);
                        }
                        else {
                            Toast.makeText(LogInActivity.this, "Dose not mach our data", Toast.LENGTH_SHORT).show();
                        }
                        inputStream.close();
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        setField();
        loadLocale();
        logIn();
    }

}

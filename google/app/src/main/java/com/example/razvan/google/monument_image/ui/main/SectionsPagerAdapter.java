package com.example.razvan.google.monument_image.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.razvan.google.model.MonumentImage;

import java.util.ArrayList;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<MonumentImage>monumentImageArrayList;
    private int position;

    public SectionsPagerAdapter(FragmentManager fm, ArrayList<MonumentImage>monumentImageArrayList, int position) {
        super(fm);
        this.monumentImageArrayList = monumentImageArrayList;
        this.position = position;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(PlaceholderFragment.POSITION, this.position);
        bundle.putString(PlaceholderFragment.IMAGE_PATH, monumentImageArrayList.get(position).getServerImagePath());
        bundle.putString(PlaceholderFragment.TITLE, String.valueOf(monumentImageArrayList.get(position).getId()));
        Fragment fragment = new PlaceholderFragment();
        fragment.setArguments(bundle);
        fragment = PlaceholderFragment.newInstance(position, String.valueOf(monumentImageArrayList.get(position).getId()), monumentImageArrayList.get(position).getServerImagePath());
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(monumentImageArrayList.get(position).getId());
    }

    @Override
    public int getCount() {
        return monumentImageArrayList.size();
    }
}
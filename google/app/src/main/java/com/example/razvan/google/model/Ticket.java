package com.example.razvan.google.model;

public class Ticket {

    public int id;
    public String qrCode;
    public String date;
    public String monumentName;

    public Ticket(int id, String qrCode, String date, String monumentName) {
        this.id = id;
        this.qrCode = qrCode;
        this.date = date;
        this.monumentName = monumentName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonumentName() {
        return monumentName;
    }

    public void setMonumentName(String monumentName) {
        this.monumentName = monumentName;
    }
}

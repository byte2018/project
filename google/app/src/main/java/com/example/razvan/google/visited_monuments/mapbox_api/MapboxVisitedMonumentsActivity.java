package com.example.razvan.google.visited_monuments.mapbox_api;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.razvan.google.MainPageActivity;
import com.example.razvan.google.PreferencesActivity;
import com.example.razvan.google.QrScannerActivity;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;
import com.example.razvan.google.model.UserInformation;
import com.example.razvan.google.monument.MonumentActivity;
import com.example.razvan.google.profile.ProfileActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.style.layers.Layer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField;

public class MapboxVisitedMonumentsActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private static final float INIT_ZOOM = 5.0f;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int HTTP_RESPONSE_OK = 200;
    public String VISITED_MONUMENTS_API_QUERY_URL = "http://192.168.137.1:8000/visited-monuments/get-all-visited-monuments/";
    public String USERS_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String VIEW_IMAGE_URL = "http://192.168.137.1:8000/user-information/view-image/";
    public String USER_INFORMATION = "http://192.168.137.1:8000/user-information/get-image-path/";
    public static final String TICKET_LIST = "Ticket List";
    private static final float DEFAULT_ZOOM = 15f;

    public MapView mapView;
    public MapboxMap map;

    private HttpClient httpClient = new DefaultHttpClient();
    private HttpResponse httpResponse;

    public JSONArray ticketListInJSONFormat;
    public int userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_mapbox_visited_monuments);

        generateUrl();
        userId = runUserQuery();

        VISITED_MONUMENTS_API_QUERY_URL += userId;
        USER_INFORMATION += userId;
        ticketListInJSONFormat = runJoinTicketQuery(VISITED_MONUMENTS_API_QUERY_URL);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        setNavigationHeaderTitles(getUserInformationFromDatabase(USER_INFORMATION));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mapView = (MapView)findViewById(R.id.visited_monuments_map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this::onMapReady);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        map = mapboxMap;

        initMap();
        loadLocale();

        map.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull Marker marker) {
                Intent change = new Intent(MapboxVisitedMonumentsActivity.this, MonumentActivity.class);
                change.putExtra("monumentName", marker.getTitle());
                change.putExtra(TICKET_LIST, ticketListInJSONFormat.toString());
                startActivity(change);
                return true;
            }
        });
    }

    private void initMap() {
        moveCamera(new LatLng(46.0,25.0), INIT_ZOOM);

        for (int index = 0; index < ticketListInJSONFormat.length(); ++index) {
            try {
                JSONObject jsonObject = ticketListInJSONFormat.getJSONObject(index);
                addMarker(new LatLng(
                        jsonObject.getDouble("longitude"),
                        jsonObject.getDouble("latitude")
                ), jsonObject.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void addMarker(LatLng latLng, String locationName) {
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(locationName);
        map.addMarker(markerOptions);
    }

    private void moveCamera(LatLng latLng, float zoom) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public String urlEncodeString(String stringToEncode) throws UnsupportedEncodingException {
        return URLEncoder.encode(stringToEncode, "UTF-8");
    }

    public void generateUrl (){
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        byte[] userEmailToEncode;
        String base64EncodedEmail;
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0, base64EncodedEmail.length() - 1);
        USERS_API_URL = USERS_API_URL + base64EncodedEmail;
    }

    private int runUserQuery() {
        HttpGet httpGet = new HttpGet(USERS_API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        HttpResponse httpResponse;
        HttpClient httpClient = new DefaultHttpClient();

        int userId = 0;

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                assert jsonObject != null;
                userId = jsonObject.getInt("id");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userId;
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public UserInformation getUserInformationFromDatabase(String API_URL) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == HTTP_RESPONSE_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        return userInformation;
    }

    @Nullable
    public JSONArray runJoinTicketQuery(String API_URL) {
        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                inputStream.close();
                return jsonArray;
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public JSONArray convertStringToJsonArray(String jsonToConvert){
        try {
            return new JSONArray(jsonToConvert);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeaderTitles(UserInformation userInformation) {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        TextView userNameView = (TextView)headerView.findViewById(R.id.user_name_nav);
        userNameView.setText(
                sharedPreferences.getString(
                        SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
        TextView userEmailView = (TextView)headerView.findViewById(R.id.user_email_nav);
        userEmailView.setText(sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, ""));
        CircleImageView profileImageView = (CircleImageView)headerView.findViewById(R.id.profile_image);
        if (userInformation != null) {
            try {
                Glide.with(getBaseContext())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(profileImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();
        sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(MapActivity.ROAD_TOLERANCE);
        editor.remove(MapActivity.TRAVEL_MODE);
        editor.apply();
        Intent change = new Intent(MapboxVisitedMonumentsActivity.this, MainPageActivity.class);
        startActivity(change);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_map) {
            startActivity(new Intent(MapboxVisitedMonumentsActivity.this, MapboxMapActivity.class));
        } else if (id == R.id.nav_account) {
            startActivity(new Intent(MapboxVisitedMonumentsActivity.this, ProfileActivity.class));
        } else if (id == R.id.nav_preferences) {
            startActivity(new Intent(MapboxVisitedMonumentsActivity.this, PreferencesActivity.class));
        } /*else if (id == R.id.nav_qr_scanner) {
            startActivity(new Intent(MapboxVisitedMonumentsActivity.this, QrScannerActivity.class));
        } */else if (id == R.id.nav_visited_monuments) {
        } else if (id == R.id.nav_language) {
            showChangeLanguageDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            logOut();
        }else if (id == R.id.action_search){
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void showChangeLanguageDialog() {
        final String[] listItems = {"English", "Roumanian", "Hindi", "Russian", "Mandarin", "French", "Hungarian", "Greek", "German"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(MapboxVisitedMonumentsActivity.this);
        builder.setTitle("Select language");
        builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        setLocale("en");
                       // setMapLocale("en");
                        recreate();
                        break;
                    case 1:
                        setLocale("ro");
                      //  setMapLocale("ro");
                        recreate();
                        break;
                    case 2:
                        setLocale("hi");
                     //   setMapLocale("hi");
                        recreate();
                        break;
                    case 3:
                        setLocale("ru");
                     //   setMapLocale("ru");
                        recreate();
                        break;
                    case 4:
                        setLocale("zh");
                      //  setMapLocale("zh");
                        recreate();
                        break;
                    case 5:
                        setLocale("fr");
                       // setMapLocale("fr");
                        recreate();
                        break;
                    case 6:
                        setLocale("hu");
                       // setMapLocale("hu");
                        recreate();
                        break;
                    case 7:
                        setLocale("el");
                        //setMapLocale("el");
                        recreate();
                        break;
                    case 8:
                        setLocale("de");
                        //setMapLocale("de");
                        recreate();
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /*public void setMapLocale(String locale) {
        Layer mapText = map.getLayer("country-label-lg");
        assert mapText != null;
        mapText.setProperties(textField("{name_" + locale + "}"));
    }*/

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

}

package com.example.razvan.google.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.MainPageActivity;
import com.example.razvan.google.PreferencesActivity;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;
import com.example.razvan.google.model.UserInformation;
import com.example.razvan.google.visited_monuments.mapbox_api.MapboxVisitedMonumentsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int GET_FROM_GALLERY = 1;
    private static final String IMAGE_POST_URL = "http://192.168.137.1:8000/user-information/add";
    public String USER_API_URL = "http://192.168.137.1:8000/user/get-email/";
    public String USER_INFORMATION = "http://192.168.137.1:8000/user-information/get-image-path/";
    public String VIEW_IMAGE_URL = "http://192.168.137.1:8000/user-information/view-image/";

    private HttpClient httpClient = new DefaultHttpClient();
    private HttpResponse httpResponse;

    public int userId;

    public FloatingActionButton uploadImage;
    public CircleImageView profileImage;
    public TextView userNameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getUserId();
        USER_INFORMATION += userId;

        setNavigationHeaderTitles(getUserInformationFromDatabase(USER_INFORMATION));

        loadLocale();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setUpFields();
        setUserName();
        getUserInformationFromDatabase(USER_INFORMATION, profileImage);
        selectImageFromGallery();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_log_out) {
            logOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    private void setNavigationHeaderTitles(UserInformation userInformation) {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        TextView userNameView = (TextView)headerView.findViewById(R.id.user_name_nav);
        userNameView.setText(
                sharedPreferences.getString(
                        SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
        TextView userEmailView = (TextView)headerView.findViewById(R.id.user_email_nav);
        userEmailView.setText(sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, ""));
        CircleImageView profileImageView = (CircleImageView)headerView.findViewById(R.id.profile_image);

        if (userInformation != null) {
            try {
                Glide.with(getBaseContext())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(profileImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    private static JSONObject convertStreamToJsonObject (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public UserInformation getUserInformationFromDatabase(String API_URL) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonObject(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }

        return userInformation;
    }

    private void selectImageFromGallery() {
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();

            String[] filePath = { MediaStore.Images.Media.DATA };
            assert selectedImage != null;
            Cursor cursor = getContentResolver().query(selectedImage, filePath, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            File image = new File(imagePath);

            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
            bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;

            cursor.close();

            uploadImageToServer(IMAGE_POST_URL, image, profileImage, this);
        }
    }

    public void uploadImageToServer(final String API_URL, final File image, final CircleImageView circleImageView, final Activity activity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String contentType = getFileType(image.getPath());
                String filePath = image.getAbsolutePath();

                OkHttpClient okHttpClient = new OkHttpClient();

                RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), image);
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", contentType)
                        .addFormDataPart("image", filePath.substring(filePath.lastIndexOf("/")+1), fileBody)
                        .addFormDataPart("user_id", String.valueOf(userId))
                        .build();

                Request request = new Request.Builder()
                        .url(API_URL)
                        .addHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE)
                                .getString(SignInActivity.ACCOUNT_API_TOKEN, ""))
                        .post(requestBody)
                        .build();

                try {
                    Response response = okHttpClient.newCall(request).execute();
                    if (response.isSuccessful()) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(), "Success", Toast.LENGTH_SHORT).show();
                                Glide.with(getBaseContext())
                                        .load(image)
                                        .into(circleImageView);
                                setActionBarImage(image);
                            }
                        });
                    }else {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(), "Error:" + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private String getFileType(String filePath) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    public void setActionBarImage(File image) {
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        CircleImageView profileImageView = (CircleImageView)navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        Glide.with(getBaseContext())
                .load(image)
                .into(profileImageView);
    }

    public void getUserId() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        String userEmail = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "");
        byte[] userEmailToEncode = userEmail.getBytes();
        String base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        USER_API_URL = USER_API_URL + base64EncodedEmail;

        HttpGet httpGet = new HttpGet(USER_API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONObject userJSON = convertStreamToJsonObject(inputStream);
                assert userJSON != null;
                userId = userJSON.getInt("id");
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    public static Bitmap convertStringToBitmap (String imageString) {
        String imageDataBytes = imageString.substring(imageString.indexOf(",") + 1);
        InputStream inputStream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));

        return BitmapFactory.decodeStream(inputStream);
    }

    @Nullable
    private static JSONObject convertStreamToJsonOblect(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getUserInformationFromDatabase(String API_URL, @NonNull CircleImageView circleImageView) {
        UserInformation userInformation = null;

        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONObject jsonObject = convertStreamToJsonOblect(inputStream);
                    if (jsonObject != null) {
                        userInformation = new UserInformation(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("user_id")
                        );
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (userInformation != null) {
            try {
                Glide.with(getBaseContext())
                        .load(VIEW_IMAGE_URL + Base64.encodeToString(URLEncoder.encode(userInformation.getImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                        .into(circleImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpFields() {
        uploadImage = findViewById(R.id.floatingActionButton);
        profileImage = findViewById(R.id.imageView1);
        userNameView = findViewById(R.id.user_name_view_id);
    }

    @SuppressLint("SetTextI18n")
    private void setUserName() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        userNameView.setText(
                sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "")
        );
    }

    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(SignInActivity.ACCOUNT_FIRST_NAME);
        editor.remove(SignInActivity.ACCOUNT_LAST_NAME);
        editor.remove(SignInActivity.ACCOUNT_EMAIL);
        editor.remove(SignInActivity.ACCOUNT_API_TOKEN);
        editor.apply();
        sharedPreferences = getSharedPreferences(MapActivity.MAP_PREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(MapActivity.ROAD_TOLERANCE);
        editor.remove(MapActivity.TRAVEL_MODE);
        editor.apply();
        Intent change = new Intent(ProfileActivity.this, MainPageActivity.class);
        startActivity(change);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_map) {
            startActivity(new Intent(ProfileActivity.this, MapboxMapActivity.class));
        } else if (id == R.id.nav_account) {
        } else if (id == R.id.nav_preferences) {
            startActivity(new Intent(ProfileActivity.this, PreferencesActivity.class));
        } /*else if (id == R.id.nav_qr_scanner) {
            startActivity(new Intent(ProfileActivity.this, QrScannerActivity.class));
        } */else if (id == R.id.nav_visited_monuments) {
            startActivity(new Intent(ProfileActivity.this, MapboxVisitedMonumentsActivity.class));
        } else if (id == R.id.nav_language) {
            showChangeLanguageDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showChangeLanguageDialog() {
        final String[] listItems = {"English", "Roumanian", "Hindi", "Russian", "Mandarin", "French", "Hungarian", "Greek", "German"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Select language");
        builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        setLocale("en");
                        recreate();
                        break;
                    case 1:
                        setLocale("ro");
                        recreate();
                        break;
                    case 2:
                        setLocale("hi");
                        recreate();
                        break;
                    case 3:
                        setLocale("ru");
                        recreate();
                        break;
                    case 4:
                        setLocale("zh");
                        recreate();
                        break;
                    case 5:
                        setLocale("fr");
                        recreate();
                        break;
                    case 6:
                        setLocale("hu");
                        recreate();
                        break;
                    case 7:
                        setLocale("el");
                        recreate();
                        break;
                    case 8:
                        setLocale("de");
                        recreate();
                        break;
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }
}

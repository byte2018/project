package com.example.razvan.google.monument;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;
import com.example.razvan.google.model.MonumentImage;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MonumentImageAdapter extends RecyclerView.Adapter<MonumentImageAdapter.MonumentImageViewHolder> {

    private static final String BASE_API_URL = "http://192.168.137.1:8000/monument-image/view-image/";

    private Context context;
    private ArrayList<MonumentImage> monumentImageList;

    public MonumentImageAdapter(Context context, ArrayList<MonumentImage> monumentImageList) {
        this.context = context;
        this.monumentImageList = monumentImageList;
    }

    @NonNull
    @Override
    public MonumentImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View view = layoutInflater.inflate(R.layout.monument_image_item, null);
        return new MonumentImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MonumentImageViewHolder holder, int position) {
        MonumentImage monumentImage = monumentImageList.get(position);
        try {
            Glide.with(context)
                    .load(BASE_API_URL + Base64.encodeToString(URLEncoder.encode(monumentImage.getServerImagePath(), "UTF-8").getBytes(), Base64.DEFAULT))
                    .override(600, 500)
                    .centerCrop()
                    .into(holder.monumentImageView);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        /*holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClicked) {
                if (!isLongClicked) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putParcelableArrayListExtra("imageList", monumentImageList);
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return monumentImageList.size();
    }

    public class MonumentImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView monumentImageView;

        //private ItemClickListener itemClickListener;

        public MonumentImageViewHolder(View itemView) {
            super(itemView);
            monumentImageView = (ImageView)itemView.findViewById(R.id.monument_image_view);

            //itemView.setOnClickListener(this);
        }

        /*public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }*/
    }

}

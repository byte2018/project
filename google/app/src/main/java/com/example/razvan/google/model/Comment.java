package com.example.razvan.google.model;

import android.content.Context;

public class Comment {

    private int id;
    private int userId;
    private String userName;
    private String comment;
    private String userProfileImagePath;
    private Context context;

    public Comment(int id, int userId, String userName, String comment, String userProfileImagePath, Context context) {
        this.id = id;
        this.userId = userId;
        this.userName = userName;
        this.comment = comment;
        this.userProfileImagePath = userProfileImagePath;
        this.context = context;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserProfileImagePath() {
        return userProfileImagePath;
    }

    public void setUserProfileImagePath(String userProfileImagePath) {
        this.userProfileImagePath = userProfileImagePath;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}

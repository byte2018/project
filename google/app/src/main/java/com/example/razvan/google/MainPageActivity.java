package com.example.razvan.google;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.razvan.google.auth.LogInActivity;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.map.google_api.MapActivity;
import com.example.razvan.google.map.mapbox_api.MapboxMapActivity;

import java.util.Locale;

public class MainPageActivity extends AppCompatActivity {

    public Button goToLogInButton;
    public Button goToSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        checkIfLoggedIn();
        loadLocale();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        goToLogInButton = (Button)findViewById(R.id.log_in);
        goToSignInButton = (Button)findViewById(R.id.sign_in);

        goToLogIn();
        goToSignIn();
    }

    private void checkIfLoggedIn() {
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(SignInActivity.ACCOUNT_EMAIL) && sharedPreferences.contains(SignInActivity.ACCOUNT_FIRST_NAME)
                && sharedPreferences.contains(SignInActivity.ACCOUNT_LAST_NAME)){
           Intent change = new Intent(MainPageActivity.this, MapboxMapActivity.class);
          startActivity(change);
       }
    }

    public void goToLogIn (){
        goToLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent change = new Intent(MainPageActivity.this, LogInActivity.class);
                startActivity(change);
            }
        });
    }

    public void goToSignIn (){
        goToSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent change = new Intent(MainPageActivity.this, SignInActivity.class);
                startActivity(change);
            }
        });
    }

    public void loadLocale (){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
        String language = sharedPreferences.getString("Language", "");
        setLocale(language);
    }

    public void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("Language", language);
        editor.apply();
    }

}

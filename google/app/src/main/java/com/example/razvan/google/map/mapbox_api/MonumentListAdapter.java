package com.example.razvan.google.map.mapbox_api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.Monument;
import com.example.razvan.google.monument.MonumentActivity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import de.hdodenhof.circleimageview.CircleImageView;

public class MonumentListAdapter extends RecyclerView.Adapter<MonumentListAdapter.MonumentListViewHolder> {

    private static final int STATUS_OK = 200;
    public static final String VISITED_MONUMENT_ADD_QUERY_URL = "http://192.168.137.1:8000/visited-monuments/add";

    private Context context;
    private ArrayList<Monument>monumentArrayList;
    private int userId;

    public MonumentListAdapter(Context context, ArrayList<Monument> monumentArrayList, int userId) {
        this.context = context;
        this.monumentArrayList = monumentArrayList;
        this.userId = userId;
    }

    @NonNull
    @Override
    public MonumentListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View view = layoutInflater.inflate(R.layout.monument_list_card_view, null);
        return new MonumentListViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MonumentListViewHolder holder, int position) {
        Monument monument = monumentArrayList.get(position);

        if (!monument.getMonumentImageUrl().equals("0")) {
            Glide.with(context)
                    .load(monument.getMonumentImageUrl())
                    .override(600, 500)
                    .centerCrop()
                    .into(holder.monumentImageCircleView);
        }
        holder.monumentNameTextView.setText(monument.getMonumentName());
        holder.monumentAverageRatingNumberTextView.setText(String.valueOf(monument.getAverageRating()));
        holder.monumentAverageRatingBar.setRating(monument.getAverageRating());
        holder.monumentLocationTextView.setText(String.valueOf(monument.getMonumentLocation().getLatitude()) + ' ' + monument.getMonumentLocation().getLongitude());
        holder.visitMonumentButton.setText((monument.isMonumentVisitedStatus()) ? "Visited" : "Visit");

        holder.visitMonumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!monument.isMonumentVisitedStatus()) {
                    List<NameValuePair> nameValuePairList = new ArrayList<>(2);
                    nameValuePairList.add(new BasicNameValuePair("user_id", String.valueOf(userId)));
                    nameValuePairList.add(new BasicNameValuePair("monument_id", String.valueOf(monument.getId())));

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(VISITED_MONUMENT_ADD_QUERY_URL);
                    httpPost.setHeader(
                            "api_token",
                            context.getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, "")
                    );

                    try {
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        if (httpResponse.getStatusLine().getStatusCode() == STATUS_OK) {
                            holder.visitMonumentButton.setText("Visited");
                            monumentArrayList.get(position).setMonumentVisitedStatus(true);
                        } else {
                            Toast.makeText(context, "Something went wrong (error: )" + httpResponse.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Already visited", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClicked) {
                Intent intent = new Intent(context, MonumentActivity.class);
                intent.putExtra("monumentName", monument.getMonumentName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return monumentArrayList.size();
    }

    class MonumentListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CircleImageView monumentImageCircleView;
        private TextView monumentNameTextView;
        private TextView monumentAverageRatingNumberTextView;
        private RatingBar monumentAverageRatingBar;
        private TextView monumentLocationTextView;
        private Button visitMonumentButton;

        private ItemClickListener itemClickListener;

        MonumentListViewHolder(View itemView) {
            super(itemView);
            monumentImageCircleView = itemView.findViewById(R.id.monument_image_circle_view);
            monumentNameTextView = itemView.findViewById(R.id.monument_name_text_view);
            monumentAverageRatingNumberTextView = itemView.findViewById(R.id.monument_average_rating_number);
            monumentAverageRatingBar = itemView.findViewById(R.id.monument_average_rating_bar);
            monumentLocationTextView = itemView.findViewById(R.id.monument_location_text_view);
            visitMonumentButton = itemView.findViewById(R.id.visit_monument_button);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }

}

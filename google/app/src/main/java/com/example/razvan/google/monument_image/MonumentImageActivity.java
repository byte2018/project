package com.example.razvan.google.monument_image;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.MonumentImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MonumentImageActivity extends AppCompatActivity {

    private static final int STATUS_OK = 200;
    private static final int GET_FROM_GALLERY = 1;
    private static final String CREATE_IMAGE_URL = "http://192.168.137.1:8000/monument-image/add";
    private String IMAGE_API_URL = "http://192.168.137.1:8000/monument-image/view-monument-images/";

    private ArrayList<MonumentImage> monumentImageArrayList = new ArrayList<>();
    MonumentImageAdapter monumentImageAdapter;

    private RecyclerView imageRecyclerView;

    private int monumentId;
    private int userId;

    private HttpClient httpClient = new DefaultHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monument_image);

        imageRecyclerView = (RecyclerView)findViewById(R.id.image_recycler_view);
        imageRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        imageRecyclerView.setHasFixedSize(true);

        monumentId = getIntent().getIntExtra("monument_id", 0);
        userId = getIntent().getIntExtra("user_id", 0);
        IMAGE_API_URL += monumentId;
        monumentImageArrayList = getImagesFromDatabase(IMAGE_API_URL);
        monumentImageAdapter = new MonumentImageAdapter(MonumentImageActivity.this, monumentImageArrayList, userId, monumentId);
        imageRecyclerView.setAdapter(monumentImageAdapter);

        FloatingActionButton uploadImageFab = (FloatingActionButton) findViewById(R.id.upload_fab);
        uploadImageFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI), GET_FROM_GALLERY);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_FROM_GALLERY && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();

            String[] filePath = { MediaStore.Images.Media.DATA };
            assert selectedImage != null;
            Cursor cursor = getContentResolver().query(selectedImage, filePath, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            File image = new File(imagePath);
            postMonumentImageToDatabase(image, this);

            cursor.close();
        }
    }

    @NonNull
    private ArrayList<MonumentImage>getImagesFromDatabase(final String API_URL) {
        ArrayList<MonumentImage>monumentImageArrayList = new ArrayList<>();
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));
        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == STATUS_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                if (httpEntity != null) {
                    InputStream inputStream = httpEntity.getContent();
                    JSONArray monumentImagePathsJsonArray = convertStreamToJsonArray(inputStream);
                    if (monumentImagePathsJsonArray != null) {
                        for (int index = 0; index < monumentImagePathsJsonArray.length(); ++index) {
                            JSONObject jsonObject = monumentImagePathsJsonArray.getJSONObject(index);
                            monumentImageArrayList.add(new MonumentImage(
                                    jsonObject.getInt("id"),
                                    jsonObject.getString("image_path"),
                                    jsonObject.getInt("monument_id"),
                                    jsonObject.getInt("user_id")
                            ));
                        }
                    }
                    inputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            httpGet.releaseConnection();
        }
        return monumentImageArrayList;
    }

    private void postMonumentImageToDatabase(@NonNull File image, Activity activity) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String contentType = getFileType(image.getPath());
                String filePath = image.getAbsolutePath();

                OkHttpClient okHttpClient = new OkHttpClient();

                RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), image);
                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("type", contentType)
                        .addFormDataPart("image", filePath.substring(filePath.lastIndexOf("/")+1), fileBody)
                        .build();

                Request request = new Request.Builder()
                        .url(CREATE_IMAGE_URL)
                        .addHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""))
                        .addHeader("user_id", String.valueOf(userId))
                        .addHeader("monument_id", String.valueOf(monumentId))
                        .post(requestBody)
                        .build();

                try {
                    Response response = okHttpClient.newCall(request).execute();
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        monumentImageArrayList.add(new MonumentImage(
                                jsonObject.getInt("id"),
                                jsonObject.getString("image_path"),
                                jsonObject.getInt("monument_id"),
                                jsonObject.getInt("user_id")
                        ));
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                monumentImageAdapter.notifyDataSetChanged();
                            }
                        });
                    }else {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(), "Error: " + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //monumentImageAdapter.notifyDataSetChanged();
    }

    private String getFileType(String path) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(path);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    @NonNull
    public static String convertBitmapToString(@NonNull Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}

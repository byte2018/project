package com.example.razvan.google;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.visited_monuments.google_api.VisitedMonumentsActivity;
import com.example.razvan.google.visited_monuments.mapbox_api.MapboxVisitedMonumentsActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class TicketActivity extends AppCompatActivity {

    public TextView monumentNameView;
    public TextView userNameView;
    public ImageView qrCodeImageView;
    public TextView dateView;

    public String qrCode;
    public String monumentName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        setUpFields();
        updateFields();

        FloatingActionButton locateObjectiveOnMap = (FloatingActionButton)findViewById(R.id.discover_loc_id);
        locateObjectiveOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TicketActivity.this, MapboxVisitedMonumentsActivity.class);
                intent.putExtra("monumentName", monumentName);
                startActivity(intent);
            }
        });
    }

    @Nullable
    private Bitmap generateQrCode(String resultCode){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

        try{
            BitMatrix bitMatrix = multiFormatWriter.encode(resultCode, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (WriterException e){
            e.printStackTrace();
        }
        return null;
    }

    private void updateFields() {
        qrCode = getIntent().getStringExtra("resultCode");
        qrCodeImageView.setImageBitmap(generateQrCode(qrCode));
        SharedPreferences sharedPreferences = getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        String userName = sharedPreferences.getString(SignInActivity.ACCOUNT_FIRST_NAME, "") + " " + sharedPreferences.getString(SignInActivity.ACCOUNT_LAST_NAME, "");
        userNameView.setText(userName);
        monumentName = getIntent().getStringExtra("monumentName");
        monumentNameView.setText(getIntent().getStringExtra("monumentName"));
        dateView.setText(getIntent().getStringExtra("currentDate"));
    }

    private void setUpFields() {
        monumentNameView = (TextView)findViewById(R.id.monu_name_view);
        userNameView = (TextView)findViewById(R.id.username_view);
        qrCodeImageView = (ImageView)findViewById(R.id.qr_code_tkt_view);
        dateView = (TextView)findViewById(R.id.date_time_h_view);
    }
}

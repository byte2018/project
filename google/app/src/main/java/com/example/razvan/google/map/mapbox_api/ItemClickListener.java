package com.example.razvan.google.map.mapbox_api;

import android.view.View;

public interface ItemClickListener {
    void onClick (View view, int position, boolean isLongClicked);
}

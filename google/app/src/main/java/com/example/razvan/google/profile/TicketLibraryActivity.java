package com.example.razvan.google.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.model.Ticket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class TicketLibraryActivity extends Fragment {

    public String API_BASE_URL = "http://192.168.137.1:8000/ticket/all-information/";
    public String USER_ID_QUERY_URL = "http://192.168.137.1:8000/user/get-email/";

    private byte[] userEmailToEncode;
    private String base64EncodedEmail;

    public RecyclerView recyclerView;
    public TicketAdapter ticketAdapter;
    public List<Ticket> ticketList = new ArrayList<>();

    public HttpClient httpClient = new DefaultHttpClient();
    public HttpGet httpGet;
    public HttpResponse httpResponse;

    public JSONArray ticketListInJsonFormat;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ticket_library_page, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);

        setUpUserQueryUrl();

        API_BASE_URL = API_BASE_URL + getUserId(USER_ID_QUERY_URL);
        executeDatabaseQuery(API_BASE_URL);

        try {
            initTicketList();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ticketAdapter = new TicketAdapter(getActivity(), ticketList);
        recyclerView.setAdapter(ticketAdapter);

        return view;
    }

    private void setUpUserQueryUrl() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        userEmailToEncode = sharedPreferences.getString(SignInActivity.ACCOUNT_EMAIL, "").getBytes();
        base64EncodedEmail = Base64.encodeToString(userEmailToEncode, Base64.DEFAULT);
        base64EncodedEmail = base64EncodedEmail.substring(0,base64EncodedEmail.length()-1);
        USER_ID_QUERY_URL = USER_ID_QUERY_URL + base64EncodedEmail;
    }

    public JSONArray convertStreamToJsonAray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void executeDatabaseQuery(String API_URL){
        httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", getActivity().getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(threadPolicy);
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                ticketListInJsonFormat = convertStreamToJsonAray(inputStream);
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initTicketList() throws JSONException {
        for (int index = 0; index <= ticketList.size(); ++index){
            JSONObject ticketInJsonFormat = ticketListInJsonFormat.getJSONObject(index);
            ticketList.add(new Ticket(
                    ticketInJsonFormat.getInt("id"),
                    ticketInJsonFormat.getString("code"),
                    ticketInJsonFormat.getString("date"),
                    ticketInJsonFormat.getString("name")
            ));
        }
    }

    @Nullable
    public JSONObject convertStreamToJsonObject (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONObject(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    private String getUserId (String API_URL){
        HttpGet httpGet = new HttpGet(API_URL);
        httpGet.setHeader("api_token", Objects.requireNonNull(getContext()).getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE)
                .getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONObject userJSON = convertStreamToJsonObject(inputStream);
                inputStream.close();
                assert userJSON != null;
                return String.valueOf(userJSON.getInt("id"));
            }
        }catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

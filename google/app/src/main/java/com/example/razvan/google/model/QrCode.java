package com.example.razvan.google.model;

import android.graphics.Bitmap;

public class QrCode {

    public String code;
    public Monument monument;
    public Bitmap qrCodeImage;

    public void setQrCodeImage(Bitmap qrCodeImage) {
        this.qrCodeImage = qrCodeImage;
    }

    public Bitmap getQrCodeImage() {
        return qrCodeImage;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setMonument(Monument monument) {
        this.monument = monument;
    }

    public Monument getMonument() {
        return monument;
    }

}

package com.example.razvan.google.visited_monuments.google_api;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.razvan.google.R;
import com.example.razvan.google.auth.SignInActivity;
import com.example.razvan.google.monument.MonumentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class VisitedMonumentsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String API_QUERY_URL = "http://192.168.137.1:8000/ticket/view-all-monuments";
    public String MONUMENT_LOCATION = "http://192.168.137.1:8000/monument/get-location/";
    public static final String TICKET_LIST = "Ticket List";
    private static final int MAX_RESULTS = 1;
    private static final float DEFAULT_ZOOM = 15f;

    HttpClient httpClient = new DefaultHttpClient();
    HttpGet httpGet = new HttpGet(API_QUERY_URL);
    HttpResponse httpResponse;

    public JSONArray ticketListInJSONFormat;
    public String monumentName;
    private LatLng monumentLocation;

    public GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visited_monuments);

        monumentName = getIntent().getStringExtra("monumentName");
        MONUMENT_LOCATION += monumentName;
        MONUMENT_LOCATION = MONUMENT_LOCATION.replaceAll(" ", "%20");

        getMonumentLocation();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initMap() {
        //TODO: change geolocation function with basic latitude and longitude object
        moveCamera(monumentLocation, DEFAULT_ZOOM, monumentName);

        runQuery();
        for (int index = 0; index < ticketListInJSONFormat.length()-1; ++index){
            try {
                JSONObject jsonObject = ticketListInJSONFormat.getJSONObject(index);
                addMarker(new LatLng(jsonObject.getDouble("longitude"), jsonObject.getDouble("latitude")), jsonObject.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void getMonumentLocation(){
        HttpGet httpGet = new HttpGet(MONUMENT_LOCATION);
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(threadPolicy);

        try{
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null) {
                InputStream inputStream = httpEntity.getContent();
                JSONArray jsonArray = convertStreamToJsonArray(inputStream);
                assert jsonArray != null;
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                monumentLocation = new LatLng(jsonObject.getDouble("longitude"), jsonObject.getDouble("latitude"));
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addMarker(LatLng latLng, String locationName) {
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(locationName);
        mMap.addMarker(markerOptions);
    }

    public void moveCamera (LatLng latLng, float zoom){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public void moveCamera (LatLng latLng, float zoom, String locationName){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(locationName).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(markerOptions);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        initMap();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent change = new Intent(VisitedMonumentsActivity.this, MonumentActivity.class);
                change.putExtra("monumentName", marker.getTitle());
                change.putExtra(TICKET_LIST, ticketListInJSONFormat.toString());
                startActivity(change);
                return true;
            }
        });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public LatLng geoLocate (String locationName){
        Geocoder geocoder = new Geocoder(VisitedMonumentsActivity.this);
        List<Address>addressList = new ArrayList<>();
        try {
            addressList = geocoder.getFromLocationName(locationName, MAX_RESULTS);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addressList.size() > 0){
            Address address = addressList.get(0);
            return new LatLng(address.getLatitude(), address.getLongitude());
        }
        else return null;
    }

    @Nullable
    private static JSONArray convertStreamToJsonArray (InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try{
            while ((line = bufferedReader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e){
                e.printStackTrace();
                throw e;
            }
        }
        JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
        try {
            return new JSONArray(jsonTokener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray convertStringToJsonArray(String jsonToConvert){
        try {
            return new JSONArray(jsonToConvert);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void runQuery() {
        httpGet.setHeader("api_token", getSharedPreferences(SignInActivity.ACCOUNT_PREFERENCES, Context.MODE_PRIVATE).getString(SignInActivity.ACCOUNT_API_TOKEN, ""));

        try {
            StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(threadPolicy);
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            if (httpEntity != null){
                InputStream inputStream = httpEntity.getContent();
                ticketListInJSONFormat = convertStreamToJsonArray(inputStream);
                inputStream.close();
            }else {
                ticketListInJSONFormat = convertStringToJsonArray(getIntent().getStringExtra(TICKET_LIST));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
